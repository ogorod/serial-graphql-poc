// Hey Emacs, this is -*- coding: utf-8 -*-

/*
eslint @typescript-eslint/no-unused-vars: [
  warn, { argsIgnorePattern: 'type|returns|of' }
]
*/

/*
eslint no-underscore-dangle: [
  'error', {
    allowAfterThis: true,
    allow: ['_key', '_id', '_rev', '_to', '_from', '_db'],
  }
]
*/

/* eslint-disable class-methods-use-this */
/* eslint-disable max-classes-per-file */
/* eslint-disable arrow-parens */
/* eslint-disable @typescript-eslint/no-use-before-define */

import 'reflect-metadata';

// import fs from 'fs';
// import path from 'path';

// import { GraphQLFormattedError } from 'graphql';
import express from 'express';

// For free TLS certificates at LetsEncrypt see e.g.:
// https://itnext.io/node-express-letsencrypt-generate-a-free-ssl-certificate-and-run-an-https-server-in-5-minutes-a730fbe528ca
// import https from 'https';
import http from 'http';

import { graphqlHTTP } from 'express-graphql';
import uWS from 'uWebSockets.js';
import { makeBehavior } from 'graphql-ws/lib/use/uWebSockets';

import {
  Arg,
  Field,
  InputType,
  Mutation,
  ObjectType,
  PubSub,
  Query,
  Resolver,
  Root,
  Subscription,
  buildSchema,
} from 'type-graphql';

import type { Publisher } from 'type-graphql';

import type { PartialDeep } from 'type-fest';

import { Fid50Link } from '@serial-graphql-poc/fid50-link';

import type {
  HmPackage as Fid50HmPackage,
  MhPackage as Fid50MhPackage,
  SetPoints as Fid50SetPointsInterface,
  State as Fid50StateInterface,
} from '@serial-graphql-poc/fid50-link';

import { config } from '@serial-graphql-poc/iso-share';

import { Container, Inject, Service } from 'typedi';

// type Context = {};

/// /b/; fid50TxPackage
/// /b/{

@ObjectType()
class Fid50TxChNSolenoids {
  @Field()
  fuel!: number;

  @Field()
  air!: number;

  @Field()
  spanA!: number;

  @Field()
  spanB!: number;

  @Field()
  purge!: number;

  @Field()
  zero!: number;

  @Field()
  vacuum!: number;

  @Field()
  test!: number;
}

@InputType()
class Fid50TxChNSolenoidsInput implements Partial<Fid50TxChNSolenoids> {
  @Field({ nullable: true })
  fuel?: number;

  @Field({ nullable: true })
  air?: number;

  @Field({ nullable: true })
  spanA?: number;

  @Field({ nullable: true })
  spanB?: number;

  @Field({ nullable: true })
  purge?: number;

  @Field({ nullable: true })
  zero?: number;

  @Field({ nullable: true })
  vacuum?: number;

  @Field({ nullable: true })
  test?: number;
}

@ObjectType()
class Fid50TxLhcGlowPlugAndHeadHeaterStatus {
  @Field()
  headHeater1!: number;

  @Field()
  headHeater2!: number;

  @Field()
  glowPlug1!: number;

  @Field()
  glowPlug2!: number;

  @Field()
  lhc1!: number;

  @Field()
  lhc2!: number;
}

/* eslint-disable brace-style, @typescript-eslint/indent */

@InputType()
class Fid50TxLhcGlowPlugAndHeadHeaterStatusInput
  implements Partial<Fid50TxLhcGlowPlugAndHeadHeaterStatus>
{
  @Field({ nullable: true })
  headHeater1?: number;

  @Field({ nullable: true })
  headHeater2?: number;

  @Field({ nullable: true })
  glowPlug1?: number;

  @Field({ nullable: true })
  glowPlug2?: number;

  @Field({ nullable: true })
  lhc1?: number;

  @Field({ nullable: true })
  lhc2?: number;
}

/* eslint-enable brace-style, @typescript-eslint/indent */

@ObjectType()
class Fid50TxGainRangeAndHtStatus {
  @Field()
  ht1!: number;

  @Field()
  ch1AmplificationStage!: number;

  @Field()
  ht2!: number;

  @Field()
  ch2AmplificationStage!: number;
}

/* eslint-disable brace-style, @typescript-eslint/indent */

@InputType()
class Fid50TxGainRangeAndHtStatusInput
  implements Partial<Fid50TxGainRangeAndHtStatus>
{
  @Field({ nullable: true })
  ht1?: number;

  @Field({ nullable: true })
  ch1AmplificationStage?: number;

  @Field({ nullable: true })
  ht2?: number;

  @Field({ nullable: true })
  ch2AmplificationStage?: number;
}

/* eslint-enable brace-style, @typescript-eslint/indent */

@ObjectType()
class Fid50TxUserDigitalOutput {
  @Field()
  isolated0!: number;

  @Field()
  isolated1!: number;

  @Field()
  isolated2!: number;

  @Field()
  isolated3!: number;

  @Field()
  nonIsolated0!: number;

  @Field()
  nonIsolated1!: number;

  @Field()
  nonIsolated2!: number;

  @Field()
  nonIsolated3!: number;
}

/* eslint-disable brace-style, @typescript-eslint/indent */

@InputType()
class Fid50TxUserDigitalOutputInput
  implements Partial<Fid50TxUserDigitalOutput>
{
  @Field({ nullable: true })
  isolated0?: number;

  @Field({ nullable: true })
  isolated1?: number;

  @Field({ nullable: true })
  isolated2?: number;

  @Field({ nullable: true })
  isolated3?: number;

  @Field({ nullable: true })
  nonIsolated0?: number;

  @Field({ nullable: true })
  nonIsolated1?: number;

  @Field({ nullable: true })
  nonIsolated2?: number;

  @Field({ nullable: true })
  nonIsolated3?: number;
}

/* eslint-enable brace-style, @typescript-eslint/indent */

@ObjectType()
class Fid50TxPackage implements Fid50HmPackage {
  esn!: number;

  @Field()
  ch1Solenoids!: Fid50TxChNSolenoids;

  @Field()
  ch2Solenoids!: Fid50TxChNSolenoids;

  @Field()
  lhcGlowPlugAndHeadHeaterStatus!: Fid50TxLhcGlowPlugAndHeadHeaterStatus;

  @Field()
  gainRangeAndHtStatus!: Fid50TxGainRangeAndHtStatus;

  @Field()
  userDigitalOutput!: Fid50TxUserDigitalOutput;

  @Field()
  ch1FidPressureSetpoint!: number;

  @Field()
  ch1CpPressureSetpoint!: number;

  @Field()
  pumpRpmSetpoint!: number;

  @Field()
  ch1FuelPressureSetpoint!: number;

  @Field()
  ch1AirPressureSetpoint!: number;

  @Field()
  glowplugCurrent!: number;

  @Field()
  ch2AirPressureSetpoint!: number;

  @Field()
  ch1ProbeTemperatureSetpoint!: number;

  @Field()
  ch1HeadTemperatureSetpoint!: number;

  @Field()
  ch2ProbeTemperatureSetpoint!: number;

  @Field()
  ch2HeadTemperatureSetpoint!: number;

  @Field()
  span1!: number;

  @Field()
  zero1!: number;

  @Field()
  span2!: number;

  @Field()
  zero2!: number;
}

@InputType()
class Fid50TxPackageInput implements PartialDeep<Fid50TxPackage> {
  @Field({ nullable: true })
  ch1Solenoids?: Fid50TxChNSolenoidsInput;

  @Field({ nullable: true })
  ch2Solenoids?: Fid50TxChNSolenoidsInput;

  @Field({ nullable: true })
  lhcGlowPlugAndHeadHeaterStatus?: Fid50TxLhcGlowPlugAndHeadHeaterStatusInput;

  @Field({ nullable: true })
  gainRangeAndHtStatus?: Fid50TxGainRangeAndHtStatusInput;

  @Field({ nullable: true })
  userDigitalOutput?: Fid50TxUserDigitalOutputInput;

  @Field({ nullable: true })
  ch1FidPressureSetpoint?: number;

  @Field({ nullable: true })
  ch1CpPressureSetpoint?: number;

  @Field({ nullable: true })
  pumpRpmSetpoint?: number;

  @Field({ nullable: true })
  ch1FuelPressureSetpoint?: number;

  @Field({ nullable: true })
  ch1AirPressureSetpoint?: number;

  @Field({ nullable: true })
  glowplugCurrent?: number;

  @Field({ nullable: true })
  ch2AirPressureSetpoint?: number;

  @Field({ nullable: true })
  ch1ProbeTemperatureSetpoint?: number;

  @Field({ nullable: true })
  ch1HeadTemperatureSetpoint?: number;

  @Field({ nullable: true })
  ch2ProbeTemperatureSetpoint?: number;

  @Field({ nullable: true })
  ch2HeadTemperatureSetpoint?: number;

  @Field({ nullable: true })
  span1?: number;

  @Field({ nullable: true })
  zero1?: number;

  @Field({ nullable: true })
  span2?: number;

  @Field({ nullable: true })
  zero2?: number;
}

/// /b/}

/// /b/; fid50SetPoints
/// /b/{

@ObjectType()
class Fid50Solenoids {
  @Field()
  fuel!: boolean;

  @Field()
  air!: boolean;

  @Field()
  spanA!: boolean;

  @Field()
  spanB!: boolean;

  @Field()
  purge!: boolean;

  @Field()
  zero!: boolean;

  @Field()
  vacuum!: boolean;

  @Field()
  test!: boolean;
}

@InputType()
class Fid50SolenoidsInput implements Partial<Fid50Solenoids> {
  @Field({ nullable: true })
  fuel?: boolean;

  @Field({ nullable: true })
  air?: boolean;

  @Field({ nullable: true })
  spanA?: boolean;

  @Field({ nullable: true })
  spanB?: boolean;

  @Field({ nullable: true })
  purge?: boolean;

  @Field({ nullable: true })
  zero?: boolean;

  @Field({ nullable: true })
  vacuum?: boolean;

  @Field({ nullable: true })
  test?: boolean;
}

@ObjectType()
class Fid50LhcGlowPlugAndHeadHeaterStatus {
  @Field()
  headHeater!: boolean;

  @Field()
  glowPlug!: boolean;

  @Field()
  lhc!: boolean;
}

/* eslint-disable brace-style, @typescript-eslint/indent */

@InputType()
class Fid50LhcGlowPlugAndHeadHeaterStatusInput
  implements Partial<Fid50LhcGlowPlugAndHeadHeaterStatus>
{
  @Field({ nullable: true })
  headHeater!: boolean;

  @Field({ nullable: true })
  glowPlug!: boolean;

  @Field({ nullable: true })
  lhc!: boolean;
}

/* eslint-enable brace-style, @typescript-eslint/indent */

@ObjectType()
class Fid50GainRangeAndHtStatus {
  @Field()
  ht!: boolean;

  @Field()
  amplificationStage!: number;
}

/* eslint-disable brace-style, @typescript-eslint/indent */

@InputType()
class Fid50GainRangeAndHtStatusInput
  implements Partial<Fid50GainRangeAndHtStatus>
{
  @Field({ nullable: true })
  ht!: boolean;

  @Field({ nullable: true })
  amplificationStage!: number;
}

/* eslint-enable brace-style, @typescript-eslint/indent */

@ObjectType()
class Fid50UserDigitalOutput {
  @Field()
  isolated0!: number;

  @Field()
  isolated1!: number;

  @Field()
  isolated2!: number;

  @Field()
  isolated3!: number;

  @Field()
  nonIsolated0!: number;

  @Field()
  nonIsolated1!: number;

  @Field()
  nonIsolated2!: number;

  @Field()
  nonIsolated3!: number;
}

/* eslint-disable brace-style, @typescript-eslint/indent */

@InputType()
class Fid50UserDigitalOutputInput implements Partial<Fid50UserDigitalOutput> {
  @Field({ nullable: true })
  isolated0?: number;

  @Field({ nullable: true })
  isolated1?: number;

  @Field({ nullable: true })
  isolated2?: number;

  @Field({ nullable: true })
  isolated3?: number;

  @Field({ nullable: true })
  nonIsolated0?: number;

  @Field({ nullable: true })
  nonIsolated1?: number;

  @Field({ nullable: true })
  nonIsolated2?: number;

  @Field({ nullable: true })
  nonIsolated3?: number;
}

/* eslint-enable brace-style, @typescript-eslint/indent */

@ObjectType()
class Fid50SetPoints implements Fid50SetPointsInterface {
  @Field()
  solenoids!: Fid50Solenoids;

  @Field()
  lhcGlowPlugAndHeadHeaterStatus!: Fid50LhcGlowPlugAndHeadHeaterStatus;

  @Field()
  gainRangeAndHtStatus!: Fid50GainRangeAndHtStatus;

  @Field()
  userDigitalOutput!: Fid50UserDigitalOutput;

  @Field()
  fidPressureSetpointMilliBar!: number;

  @Field()
  cpPressureSetpointMilliBar!: number;

  @Field()
  pumpRpmSetpoint!: number;

  @Field()
  fuelPressureSetpointBar!: number;

  @Field()
  airPressureSetpointBar!: number;

  @Field()
  glowplugCurrentAmpere!: number;

  @Field()
  probeTemperatureSetpointDegC!: number;

  @Field()
  headTemperatureSetpointDegC!: number;

  @Field()
  span!: number;

  @Field()
  zero!: number;
}

@InputType()
class Fid50SetPointsInput implements PartialDeep<Fid50SetPointsInterface> {
  @Field({ nullable: true })
  solenoids!: Fid50SolenoidsInput;

  @Field({ nullable: true })
  lhcGlowPlugAndHeadHeaterStatus!: Fid50LhcGlowPlugAndHeadHeaterStatusInput;

  @Field({ nullable: true })
  gainRangeAndHtStatus!: Fid50GainRangeAndHtStatusInput;

  @Field({ nullable: true })
  userDigitalOutput!: Fid50UserDigitalOutputInput;

  @Field({ nullable: true })
  fidPressureSetpointMilliBar!: number;

  @Field({ nullable: true })
  cpPressureSetpointMilliBar!: number;

  @Field({ nullable: true })
  pumpRpmSetpoint!: number;

  @Field({ nullable: true })
  fuelPressureSetpointBar!: number;

  @Field({ nullable: true })
  airPressureSetpointBar!: number;

  @Field({ nullable: true })
  glowplugCurrentAmpere!: number;

  @Field({ nullable: true })
  probeTemperatureSetpointDegC!: number;

  @Field({ nullable: true })
  headTemperatureSetpointDegC!: number;

  @Field({ nullable: true })
  span!: number;

  @Field({ nullable: true })
  zero!: number;
}

/// /b/}

/// /b/; fid50RxPackage
/// /b/{

@ObjectType()
class Fid50RxGasPressureFaults {
  @Field()
  fuel!: number;

  @Field()
  air!: number;

  @Field()
  spanA!: number;

  @Field()
  spanB!: number;

  @Field()
  purge!: number;

  @Field()
  zero!: number;

  @Field()
  vacuum!: number;

  _value!: number;
}

@ObjectType()
class Fid50RxProbeAndHeadTcFaults {
  @Field()
  probe1!: number;

  @Field()
  probe2!: number;

  @Field()
  head1!: number;

  @Field()
  head2!: number;

  _value!: number;
}

@ObjectType()
class Fid50RxExhaustTcFaults {
  @Field()
  exhaust1!: number;

  @Field()
  exhaust2!: number;

  @Field()
  safeState!: number;

  _value!: number;
}

@ObjectType()
class Fid50RxCh1SolenoidsFaults {
  @Field()
  fuel!: number;

  @Field()
  air!: number;

  @Field()
  spanA!: number;

  @Field()
  spanB!: number;

  @Field()
  purge!: number;

  @Field()
  zero!: number;

  @Field()
  vacuum!: number;

  @Field()
  test!: number;

  _value!: number;
}

@ObjectType()
class Fid50RxCh2SolenoidsFaults {
  @Field()
  fuel!: number;

  @Field()
  air!: number;

  @Field()
  spanA!: number;

  @Field()
  spanB!: number;

  @Field()
  purge!: number;

  @Field()
  zero!: number;

  @Field()
  vacuum!: number;

  @Field()
  test!: number;

  _value!: number;
}

@ObjectType()
class Fid50RxUserDigitalInputs {
  @Field()
  isolated0!: number;

  @Field()
  isolated1!: number;

  @Field()
  isolated2!: number;

  @Field()
  isolated3!: number;

  @Field()
  nonIsolated0!: number;

  @Field()
  nonIsolated1!: number;

  @Field()
  nonIsolated2!: number;

  @Field()
  nonIsolated3!: number;

  @Field()
  nonIsolatedValue!: number;

  @Field()
  isolatedValue!: number;

  _value!: number;
}

@ObjectType()
class Fid50RxPackage implements Fid50MhPackage {
  esn!: number;
  esnLow!: number;
  esnHigh!: number;

  @Field()
  gasPressureFaults!: Fid50RxGasPressureFaults;

  @Field()
  probeAndHeadTcFaults!: Fid50RxProbeAndHeadTcFaults;

  @Field()
  exhaustTcFaults!: Fid50RxExhaustTcFaults;

  @Field()
  ch1SolenoidsFaults!: Fid50RxCh1SolenoidsFaults;

  @Field()
  ch2SolenoidsFaults!: Fid50RxCh2SolenoidsFaults;

  @Field()
  userDigitalInputs!: Fid50RxUserDigitalInputs;

  @Field()
  ch1ExhaustTemperature!: number;

  @Field()
  ch1HeadTemperature!: number;

  @Field()
  ch1OutputPpm!: number;

  @Field()
  ch1CpPressure!: number;

  @Field()
  ch1FidPressure!: number;

  @Field()
  ch1CpBleed!: number;

  @Field()
  ch1FidBleed!: number;

  @Field()
  ch1GlowPlugCurrent!: number;

  @Field()
  ch2ExhaustTemperature!: number;

  @Field()
  ch2HeadTemperature!: number;

  @Field()
  ch2OutputV!: number;

  @Field()
  pumpRpm!: number;

  @Field()
  fanRpm!: number;

  @Field()
  ch2CpBleed!: number;

  @Field()
  ch2FidBleed!: number;

  @Field()
  ch2GlowPlugCurrent!: number;

  @Field()
  ch1ProbeTemperature!: number;

  @Field()
  ch2ProbeTemperature!: number;

  @Field()
  ch1FuelPressure!: number;

  @Field()
  ch1FuelBleed!: number;

  @Field()
  ch1AirPressure!: number;

  @Field()
  ch1AirBleed!: number;

  @Field()
  zeroOffset!: number;

  @Field()
  maxFactor!: number;

  @Field()
  ambientTemperature!: number;

  @Field()
  ch1ProbeDrivePercent!: number;

  @Field()
  ch2ProbeDrivePercent!: number;

  @Field()
  ch1HeadDrivePercent!: number;

  @Field()
  mainsFrequency!: number;

  @Field()
  outputPpm!: number;

  @Field()
  mainPcbUpTimeSecond!: number;

  @Field()
  pumpPcbUpTimeSecond!: number;

  @Field()
  pumpTotalRunningTimeMinute!: number;

  @Field()
  firmwareVersionMajor!: number;

  @Field()
  firmwareVersionMinor!: number;
}

/// /b/}

/// /b/; fid50State
/// /b/{

@ObjectType()
class Fid50GasPressureFaults {
  @Field()
  fuel!: boolean;

  @Field()
  air!: boolean;

  @Field()
  spanA!: boolean;

  @Field()
  spanB!: boolean;

  @Field()
  purge!: boolean;

  @Field()
  zero!: boolean;

  @Field()
  vacuum!: boolean;
}

@ObjectType()
class Fid50ProbeAndHeadTcFaults {
  @Field()
  probe!: boolean;

  @Field()
  head!: boolean;
}

@ObjectType()
class Fid50ExhaustTcFaults {
  @Field()
  exhaust!: boolean;

  @Field()
  safeState!: boolean;
}

@ObjectType()
class Fid50SolenoidsFaults {
  @Field()
  fuel!: boolean;

  @Field()
  air!: boolean;

  @Field()
  spanA!: boolean;

  @Field()
  spanB!: boolean;

  @Field()
  purge!: boolean;

  @Field()
  zero!: boolean;

  @Field()
  vacuum!: boolean;

  @Field()
  test!: boolean;
}

@ObjectType()
class Fid50UserDigitalInputs {
  @Field()
  isolated0!: number;

  @Field()
  isolated1!: number;

  @Field()
  isolated2!: number;

  @Field()
  isolated3!: number;

  @Field()
  nonIsolated0!: number;

  @Field()
  nonIsolated1!: number;

  @Field()
  nonIsolated2!: number;

  @Field()
  nonIsolated3!: number;

  @Field()
  nonIsolatedValue!: number;

  @Field()
  isolatedValue!: number;
}

@ObjectType()
class Fid50State implements Fid50StateInterface {
  @Field()
  gasPressureFaults!: Fid50GasPressureFaults;

  @Field()
  probeAndHeadTcFaults!: Fid50ProbeAndHeadTcFaults;

  @Field()
  exhaustTcFaults!: Fid50ExhaustTcFaults;

  @Field()
  solenoidsFaults!: Fid50SolenoidsFaults;

  @Field()
  userDigitalInputs!: Fid50UserDigitalInputs;

  @Field()
  exhaustTemperatureDegC!: number;

  @Field()
  headTemperatureDegC!: number;

  @Field()
  cpPressureMilliBar!: number;

  @Field()
  fidPressureMilliBar!: number;

  @Field()
  cpBleedPercent!: number;

  @Field()
  fidBleedPercent!: number;

  @Field()
  glowPlugCurrentAmpere!: number;

  @Field()
  pumpRpm!: number;

  @Field()
  fanRpm!: number;

  @Field()
  probeTemperatureDegC!: number;

  @Field()
  fuelPressureBar!: number;

  @Field()
  fuelBleedPercent!: number;

  @Field()
  airPressureBar!: number;

  @Field()
  airBleedPercent!: number;

  @Field()
  ambientTemperatureDegC!: number;

  @Field()
  probeDrivePercent!: number;

  @Field()
  headDrivePercent!: number;

  @Field()
  mainsFrequencyHz!: number;

  @Field()
  outputPpm!: number;

  @Field()
  mainPcbUpTimeSecond!: number;

  @Field()
  pumpPcbUpTimeSecond!: number;

  @Field()
  pumpTotalRunningTimeMinute!: number;

  @Field()
  firmwareVersionMajor!: number;

  @Field()
  firmwareVersionMinor!: number;
}

/// /b/}

@ObjectType()
class CastleCounter {
  constructor() {
    this.count = 0;
  }

  @Field()
  count: number;
}

@Service()
@Resolver()
class Castle {
  constructor(
    @Inject('fid50-link')
    private readonly _fid50Link: Fid50Link,
  ) {}

  @Query((_returns) => String)
  async sayHello(
    @Arg('myMame') myMame: string,
    @Arg('clientId') clientId: string,
  ): Promise<string> {
    const now = new Date();

    return `${now.toISOString()} Hello ${myMame} @ ${clientId}`;
  }

  /// /b/; castleCounter
  /// /b/{

  @Query((_returns) => CastleCounter)
  async castleCounter(): Promise<CastleCounter> {
    return this._counter;
  }

  @Mutation((_returns) => CastleCounter)
  async castleCounterIncrement(
    @PubSub('castleCounterChanged') publish: Publisher<CastleCounter>,
  ): Promise<CastleCounter> {
    console.log('in castleCounterIncrement()');
    this._counter.count += 1;
    await publish(this._counter);
    return this._counter;
  }

  @Mutation((_returns) => CastleCounter)
  async castleCounterDecrement(
    @PubSub('castleCounterChanged') publish: Publisher<CastleCounter>,
  ): Promise<CastleCounter> {
    console.log('in castleCounterDecrement()');
    this._counter.count -= 1;
    await publish(this._counter);
    return this._counter;
  }

  @Subscription({ topics: 'castleCounterChanged' })
  castleCounterChanged(@Root() payload: CastleCounter): CastleCounter {
    console.log('in castleCounterChanged()');
    return payload;
  }

  private _counter = new CastleCounter();

  /// /b/}

  /// /b/; fid50RxPackage and fid50State
  /// /b/{

  @Query((_returns) => Fid50RxPackage)
  async fid50RxPackage(): Promise<Fid50RxPackage> {
    return this._fid50Link.rxPackage;
  }

  @Query((_returns) => Fid50State)
  async fid50State(): Promise<Fid50State> {
    return this._fid50Link.state;
  }

  /// /b/}

  /// /b/; fid50RxPackage and fid50SetPoints
  /// /b/{

  @Mutation((_returns) => Fid50TxPackage)
  async setFid50TxPackage(
    @Arg('txPackage') txPackage: Fid50TxPackageInput,
  ): Promise<Fid50TxPackage> {
    this._fid50Link.txPackagePartialCopy(txPackage);
    return this._fid50Link.txPackage;
  }

  @Query((_returns) => Fid50TxPackage)
  async fid50TxPackage(): Promise<Fid50TxPackage> {
    return this._fid50Link.txPackage;
  }

  @Mutation((_returns) => Fid50SetPoints)
  async setFid50SetPoints(
    @Arg('setPoints') setPoints: Fid50SetPointsInput,
  ): Promise<Fid50SetPoints> {
    this._fid50Link.setPointsPartialCopy(setPoints);
    return this._fid50Link.setPoints;
  }

  @Query((_returns) => Fid50SetPoints)
  async fid50SetPoints(): Promise<Fid50SetPoints> {
    return this._fid50Link.setPoints;
  }

  /// /b/}
}

export default async function main(): Promise<void> {
  const schema = await buildSchema({
    resolvers: [Castle],
    container: Container,
  });

  const {
    wsProtocol,
    wsHost,
    wsPort,
    wsPath,
    httpProtocol,
    httpHost,
    httpPort,
    httpPath,
  } = config.castle;

  const httpUrl = `${httpProtocol}//${httpHost}:${httpPort}${httpPath}`;
  const wsUrl = `${wsProtocol}//${wsHost}:${wsPort}${wsPath}`;

  const app = express();
  app.use(
    httpPath,
    graphqlHTTP({
      schema,
      graphiql: true,
      // pretty: true,
      // TODO: as of express-graphql@0.12.0, subscriptions in graphiql do not
      //       work, check it at next versions.
      // graphiql: {
      //   subscriptionEndpoint: `${wsHref}`,
      // },
    }),
  );

  // const key = fs.readFileSync(
  //   path.join(projectRoot, 'certs/selfsigned.key'),
  //   'ascii',
  // );

  // const cert = fs.readFileSync(
  //   path.join(projectRoot, 'certs/selfsigned.crt'),
  //   'ascii',
  // );

  // const options = { key, cert };

  // const server = https.createServer(options, app);

  const server = http.createServer(app);

  const fid50Link = new Fid50Link();
  Container.set('fid50-link', fid50Link);

  const subscription = fid50Link.connected$.subscribe((connected) => {
    if (connected) {
      console.log('FID 50 connected');

      server.listen(httpPort, () => {
        console.log(`HTTP server ready at ${httpUrl}`);

        uWS
          .App()
          .ws(config.castle.wsPath, makeBehavior({ schema }))
          .listen(config.castle.wsPort, (listenSocket) => {
            if (listenSocket) {
              console.log(`WebSockets server ready at ${wsUrl}`);
            }
          });
      });

      subscription.unsubscribe();
    } else {
      console.log('FID 50 disconnected');
    }
  });

  // process.addListener('SIGINT', fid50Link.close);
  // process.addListener('SIGTERM', fid50Link.close);

  // server.listen(httpPort, () => {
  //   console.log(`HTTP server ready at ${httpUrl}`);

  //   uWS
  //     .App()
  //     .ws(config.castle.wsPath, makeBehavior({ schema }))
  //     .listen(config.castle.wsPort, (listenSocket) => {
  //       if (listenSocket) {
  //         console.log(`WebSockets server ready at ${wsUrl}`);
  //       }
  //     });
  // });
}
