// Hey Emacs, this is -*- coding: utf-8 -*-

import * as path from 'path';

export const projectRoot = path.join(path.resolve(__dirname), '..');
