// Hey Emacs, this is -*- coding: utf-8 -*-

import { pipeline } from 'stream';
import type { Duplex } from 'stream';

import { BehaviorSubject, Subject } from 'rxjs';
import type { Observable } from 'rxjs';
import cloneDeep from 'lodash.clonedeep';
import extend from 'just-extend';

import type { ReadonlyDeep } from 'type-fest';

import SerialPort from 'serialport';
import type { ErrorCallback } from 'serialport';

import { Encoder, hmPackageInitial } from '~/fid50-encoder';
import type { HmPackage, HmPackagePartial } from '~/fid50-encoder';

import { Decoder, mhPackageInitial } from '~/fid50-decoder';
import type { MhPackage } from '~/fid50-decoder';

import { stateInitial, stateUpdate } from '~/fid50-state';
import type { State } from '~/fid50-state';

import {
  hmPackageUpdate,
  setPointsInitial,
  setPointsUpdate,
} from '~/fid50-set-points';

import type { SetPoints, SetPointsPartial } from '~/fid50-set-points';

export type { SetPoints, State };
export type { HmPackage, MhPackage };

export class Fid50Link {
  constructor(
    readonly portPath = '/dev/ttyS0',
    readonly portBaudRate = 19200,
    readonly txIntervalMilliSecond = 100,
  ) {
    this._port = new SerialPort(
      this.portPath,
      { baudRate: this.portBaudRate },
      this.onOpenHandler,
    );
  }

  // /b/; behaviour
  // /b/{

  get connected(): boolean {
    return this._connected$.getValue();
  }

  get connected$(): Observable<boolean> {
    return this._connected$;
  }

  private readonly _connected$ = new BehaviorSubject(false);

  txPackagePartialCopy(txPackagePartial: ReadonlyDeep<HmPackagePartial>): void {
    extend(this._txPackage, txPackagePartial);
    setPointsUpdate(this._setPoints, this._txPackage);
    this._txPackage$.next(this._txPackage);
    this._setPoints$.next(this._setPoints);
  }

  set txPackage(txPackage: HmPackage) {
    this._txPackage = txPackage;
    setPointsUpdate(this._setPoints, this._txPackage);
    this._txPackage$.next(txPackage);
    this._setPoints$.next(this._setPoints);
  }

  get txPackage(): ReadonlyDeep<HmPackage> {
    return this._txPackage;
  }

  get txPackage$(): Observable<ReadonlyDeep<HmPackage>> {
    return this._txPackage$;
  }

  private readonly _txPackage$ = new Subject<ReadonlyDeep<HmPackage>>();

  private _txPackage = cloneDeep(hmPackageInitial);

  get setPoints$(): Observable<ReadonlyDeep<SetPoints>> {
    return this._setPoints$;
  }

  private readonly _setPoints$ = new Subject<ReadonlyDeep<SetPoints>>();

  setPointsPartialCopy(setPointsPartial: ReadonlyDeep<SetPointsPartial>): void {
    extend(this._setPoints, setPointsPartial);
    hmPackageUpdate(this._txPackage, setPointsPartial);
    this._txPackage$.next(this._txPackage);
    this._setPoints$.next(this._setPoints);
  }

  set setPoints(setPoints: SetPoints) {
    this._setPoints = setPoints;
    hmPackageUpdate(this._txPackage, this._setPoints);
    this._txPackage$.next(this._txPackage);
    this._setPoints$.next(this._setPoints);
  }

  get setPoints(): ReadonlyDeep<SetPoints> {
    return this._setPoints;
  }

  private _setPoints = setPointsUpdate(
    cloneDeep(setPointsInitial),
    this._txPackage,
  );

  get txPackageSent$(): Observable<ReadonlyDeep<HmPackage>> {
    return this._txPackageSent$;
  }

  private readonly _txPackageSent$ = new Subject<ReadonlyDeep<HmPackage>>();

  get rxPackage(): ReadonlyDeep<MhPackage> {
    return this._rxPackageReceived$.getValue();
  }

  get rxPackageReceived$(): Observable<ReadonlyDeep<MhPackage>> {
    return this._rxPackageReceived$;
  }

  private readonly _rxPackageReceived$ = new BehaviorSubject<
    ReadonlyDeep<MhPackage> // eslint-disable-line @typescript-eslint/indent
  >(cloneDeep(mhPackageInitial));

  get state(): ReadonlyDeep<State> {
    return this._state$.getValue();
  }

  get state$(): Observable<ReadonlyDeep<State>> {
    return this._state$;
  }

  private readonly _state$ = new BehaviorSubject(cloneDeep(stateInitial));

  close = (): void => {
    if (this._txTimer) {
      clearInterval(this._txTimer);
    }
    if (this._port.isOpen) {
      this._port.close();
    }
  };

  // /b/}

  private _txTimer?: ReturnType<typeof setInterval>;

  private readonly txHandler = (): void => {
    if (!this._connected$.getValue()) {
      return;
    }

    this._encoder.write(this._txPackage);
    this._txPackageSent$.next(this._txPackage);
  };

  private readonly onOpenHandler: ErrorCallback = (portOpenError): void => {
    if (portOpenError) {
      console.error(`error while opening ${this.portPath}`);
    } else {
      pipeline(
        this._encoder,
        this._port as Duplex,
        this._decoder,
        (pipelineError) => {
          this._connected$.next(false);
          console.log('FID50 pipline closed:', pipelineError);
        },
      );

      this._txTimer = setInterval(this.txHandler, this.txIntervalMilliSecond);

      this._decoder.on('data', (mhPackage: MhPackage) => {
        const state = this._state$.getValue();
        stateUpdate(state, mhPackage);
        this._rxPackageReceived$.next(mhPackage);
        this._state$.next(state);

        // console.log('Module-to-Host:', mhPackage);
      });

      this._connected$.next(true);
    }
  };

  private readonly _port: SerialPort;
  private readonly _decoder = new Decoder();
  private readonly _encoder = new Encoder();
}
