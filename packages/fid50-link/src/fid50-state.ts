// Hey Emacs, this is -*- coding: utf-8 -*-

import type { ReadonlyDeep } from 'type-fest';

import type { MhPackage } from '~/fid50-decoder';

const stateInitialMutable = {
  gasPressureFaults: {
    fuel: false,
    air: false,
    spanA: false,
    spanB: false,
    purge: false,
    zero: false,
    vacuum: false,
  },
  probeAndHeadTcFaults: {
    probe: false,
    head: false,
  },
  exhaustTcFaults: {
    exhaust: false,
    safeState: false,
  },
  solenoidsFaults: {
    fuel: false,
    air: false,
    spanA: false,
    spanB: false,
    purge: false,
    zero: false,
    vacuum: false,
    test: false,
  },
  userDigitalInputs: {
    isolated0: 0,
    isolated1: 0,
    isolated2: 0,
    isolated3: 0,
    nonIsolated0: 0,
    nonIsolated1: 0,
    nonIsolated2: 0,
    nonIsolated3: 0,
    nonIsolatedValue: 0,
    isolatedValue: 0,
  },
  exhaustTemperatureDegC: 0,
  headTemperatureDegC: 0,
  cpPressureMilliBar: 0,
  fidPressureMilliBar: 0,
  cpBleedPercent: 0,
  fidBleedPercent: 0,
  glowPlugCurrentAmpere: 0,
  pumpRpm: 0,
  fanRpm: 0,
  probeTemperatureDegC: 0,
  fuelPressureBar: 0,
  fuelBleedPercent: 0,
  airPressureBar: 0,
  airBleedPercent: 0,
  ambientTemperatureDegC: 0,
  probeDrivePercent: 0,
  headDrivePercent: 0,
  mainsFrequencyHz: 0,
  outputPpm: 0,
  mainPcbUpTimeSecond: 0,
  pumpPcbUpTimeSecond: 0,
  pumpTotalRunningTimeMinute: 0,
  firmwareVersionMajor: 0,
  firmwareVersionMinor: 0,
};

export type State = typeof stateInitialMutable;

export const stateInitial: ReadonlyDeep<State> = stateInitialMutable;

const ADC_MAX = 2 ** 16;

const stateCoefInitialMutable = {
  exhaustTemperatureDegC: 500 / ADC_MAX,
  headTemperatureDegC: 500 / ADC_MAX,
  cpPressureMilliBar: 1041.67 / ADC_MAX,
  fidPressureMilliBar: 1041.67 / ADC_MAX,
  cpBleedPercent: 100 / ADC_MAX,
  fidBleedPercent: 100 / ADC_MAX,
  glowPlugCurrentAmpere: 5 / ADC_MAX,
  probeTemperatureDegC: 500 / ADC_MAX,
  fuelPressureBar: 5 / ADC_MAX,
  fuelBleedPercent: 100 / ADC_MAX,
  airPressureBar: 5 / ADC_MAX,
  airBleedPercent: 100 / ADC_MAX,
  ambientTemperatureDegC: 50 / ADC_MAX,
  probeDrivePercent: 100 / ADC_MAX,
  headDrivePercent: 100 / ADC_MAX,
};

export type StateCoef = typeof stateCoefInitialMutable;

export const stateCoefInitial: ReadonlyDeep<StateCoef> =
  stateCoefInitialMutable;

export const stateUpdate = (
  state: State,
  mhPackage: ReadonlyDeep<MhPackage>,
  coef: ReadonlyDeep<StateCoef> = stateCoefInitial,
): State => {
  state.gasPressureFaults.fuel = Boolean(mhPackage.gasPressureFaults.fuel);
  state.gasPressureFaults.air = Boolean(mhPackage.gasPressureFaults.air);
  state.gasPressureFaults.spanA = Boolean(mhPackage.gasPressureFaults.spanA);
  state.gasPressureFaults.spanB = Boolean(mhPackage.gasPressureFaults.spanB);
  state.gasPressureFaults.purge = Boolean(mhPackage.gasPressureFaults.purge);
  state.gasPressureFaults.zero = Boolean(mhPackage.gasPressureFaults.zero);
  state.gasPressureFaults.vacuum = Boolean(mhPackage.gasPressureFaults.vacuum);

  state.probeAndHeadTcFaults.probe = Boolean(
    mhPackage.probeAndHeadTcFaults.probe1,
  );
  state.probeAndHeadTcFaults.head = Boolean(
    mhPackage.probeAndHeadTcFaults.head1,
  );

  state.exhaustTcFaults.exhaust = Boolean(mhPackage.exhaustTcFaults.exhaust1);
  state.exhaustTcFaults.safeState = Boolean(
    mhPackage.exhaustTcFaults.safeState,
  );

  state.solenoidsFaults.fuel = Boolean(mhPackage.ch1SolenoidsFaults.fuel);
  state.solenoidsFaults.air = Boolean(mhPackage.ch1SolenoidsFaults.air);
  state.solenoidsFaults.spanA = Boolean(mhPackage.ch1SolenoidsFaults.spanA);
  state.solenoidsFaults.spanB = Boolean(mhPackage.ch1SolenoidsFaults.spanB);
  state.solenoidsFaults.purge = Boolean(mhPackage.ch1SolenoidsFaults.purge);
  state.solenoidsFaults.zero = Boolean(mhPackage.ch1SolenoidsFaults.zero);
  state.solenoidsFaults.vacuum = Boolean(mhPackage.ch1SolenoidsFaults.vacuum);
  state.solenoidsFaults.test = Boolean(mhPackage.ch1SolenoidsFaults.test);

  state.userDigitalInputs.isolated0 = mhPackage.userDigitalInputs.isolated0;
  state.userDigitalInputs.isolated1 = mhPackage.userDigitalInputs.isolated1;
  state.userDigitalInputs.isolated2 = mhPackage.userDigitalInputs.isolated2;
  state.userDigitalInputs.isolated3 = mhPackage.userDigitalInputs.isolated3;
  state.userDigitalInputs.nonIsolated0 =
    mhPackage.userDigitalInputs.nonIsolated0;
  state.userDigitalInputs.nonIsolated1 =
    mhPackage.userDigitalInputs.nonIsolated1;
  state.userDigitalInputs.nonIsolated2 =
    mhPackage.userDigitalInputs.nonIsolated2;
  state.userDigitalInputs.nonIsolated3 =
    mhPackage.userDigitalInputs.nonIsolated3;
  state.userDigitalInputs.nonIsolatedValue =
    mhPackage.userDigitalInputs.nonIsolatedValue;
  state.userDigitalInputs.isolatedValue =
    mhPackage.userDigitalInputs.isolatedValue;

  state.exhaustTemperatureDegC =
    mhPackage.ch1ExhaustTemperature * coef.exhaustTemperatureDegC;
  state.headTemperatureDegC =
    mhPackage.ch1HeadTemperature * coef.headTemperatureDegC;
  state.cpPressureMilliBar = mhPackage.ch1CpPressure * coef.cpPressureMilliBar;
  state.fidPressureMilliBar =
    mhPackage.ch1FidPressure * coef.fidPressureMilliBar;
  state.cpBleedPercent = mhPackage.ch1CpBleed * coef.cpBleedPercent;
  state.fidBleedPercent = mhPackage.ch1FidBleed * coef.fidBleedPercent;
  state.glowPlugCurrentAmpere =
    mhPackage.ch1GlowPlugCurrent * coef.glowPlugCurrentAmpere;
  state.pumpRpm = mhPackage.pumpRpm;
  state.fanRpm = mhPackage.fanRpm;
  state.probeTemperatureDegC =
    mhPackage.ch1ProbeTemperature * coef.probeTemperatureDegC;
  state.fuelPressureBar = mhPackage.ch1FuelPressure * coef.fuelPressureBar;
  state.fuelBleedPercent = mhPackage.ch1FuelBleed * coef.fuelBleedPercent;
  state.airPressureBar = mhPackage.ch1AirPressure * coef.airPressureBar;
  state.airBleedPercent = mhPackage.ch1AirBleed * coef.airBleedPercent;
  state.ambientTemperatureDegC =
    mhPackage.ambientTemperature * coef.ambientTemperatureDegC;
  state.probeDrivePercent =
    mhPackage.ch1ProbeDrivePercent * coef.probeDrivePercent;
  state.headDrivePercent =
    mhPackage.ch1HeadDrivePercent * coef.headDrivePercent;
  state.mainsFrequencyHz = mhPackage.mainsFrequency;
  state.outputPpm = mhPackage.outputPpm;
  state.mainPcbUpTimeSecond = mhPackage.mainPcbUpTimeSecond;
  state.pumpPcbUpTimeSecond = mhPackage.pumpPcbUpTimeSecond;
  state.pumpTotalRunningTimeMinute = mhPackage.pumpTotalRunningTimeMinute;

  state.firmwareVersionMajor = mhPackage.firmwareVersionMajor;
  state.firmwareVersionMinor = mhPackage.firmwareVersionMinor;

  return state;
};
