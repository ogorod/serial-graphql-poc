// Hey Emacs, this is -*- coding: utf-8 -*-

import {
  MhPacket as Fid50MhPacket,
  HmPacket as Fid50HmPacket,
} from '~/fid50-packets';

const fid50MhPacket = new Fid50MhPacket();
const fid50HmPacket = new Fid50HmPacket();

console.log('FID50 Host-to-Module:', fid50MhPacket.toJSON(), '\n');
console.log('FID50 Module-to-Host:', fid50HmPacket.toJSON(), '\n');
