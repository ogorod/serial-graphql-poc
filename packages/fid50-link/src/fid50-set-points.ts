// Hey Emacs, this is -*- coding: utf-8 -*-

import type { ReadonlyDeep, PartialDeep } from 'type-fest';

import type { HmPackage } from '~/fid50-encoder';

const setPointsInitialMutable = {
  solenoids: {
    fuel: false,
    air: false,
    spanA: false,
    spanB: false,
    purge: false,
    zero: false,
    vacuum: false,
    test: false,
  },
  lhcGlowPlugAndHeadHeaterStatus: {
    headHeater: false,
    glowPlug: false,
    lhc: false,
  },
  gainRangeAndHtStatus: {
    ht: false,
    amplificationStage: 0,
  },
  userDigitalOutput: {
    isolated0: 0,
    isolated1: 0,
    isolated2: 0,
    isolated3: 0,
    nonIsolated0: 0,
    nonIsolated1: 0,
    nonIsolated2: 0,
    nonIsolated3: 0,
  },
  fidPressureSetpointMilliBar: 0,
  cpPressureSetpointMilliBar: 0,
  pumpRpmSetpoint: 0,
  fuelPressureSetpointBar: 0,
  airPressureSetpointBar: 0,
  glowplugCurrentAmpere: 0,
  probeTemperatureSetpointDegC: 0,
  headTemperatureSetpointDegC: 0,
  span: 0,
  zero: 0,
};

export type SetPoints = typeof setPointsInitialMutable;
export type SetPointsPartial = PartialDeep<SetPoints>;

export const setPointsInitial: ReadonlyDeep<SetPoints> =
  setPointsInitialMutable;

export const DAC_MAX = 2 ** 8;

const setPointsCoefInitialMutable = {
  fidPressureSetpointMilliBar: 1041.67 / DAC_MAX,
  cpPressureSetpointMilliBar: 1041.67 / DAC_MAX,
  fuelPressureSetpointBar: 5 / DAC_MAX,
  airPressureSetpointBar: 5 / DAC_MAX,
  glowplugCurrentAmpere: 5.1 / DAC_MAX,
  probeTemperatureSetpointDegC: 500 / DAC_MAX,
  headTemperatureSetpointDegC: 500 / DAC_MAX,
};

export type SetPointsCoef = typeof setPointsCoefInitialMutable;

export const setPointsCoefInitial: ReadonlyDeep<SetPointsCoef> =
  setPointsCoefInitialMutable;

export const hmPackageUpdate = (
  hmPackage: HmPackage,
  setPoints: ReadonlyDeep<SetPointsPartial>,
  coef: ReadonlyDeep<SetPointsCoef> = setPointsCoefInitial,
): HmPackage => {
  if (setPoints.solenoids !== undefined) {
    if (setPoints.solenoids.fuel !== undefined) {
      hmPackage.ch1Solenoids.fuel = Number(setPoints.solenoids.fuel);
    }
    if (setPoints.solenoids.air !== undefined) {
      hmPackage.ch1Solenoids.air = Number(setPoints.solenoids.air);
    }
    if (setPoints.solenoids.spanA !== undefined) {
      hmPackage.ch1Solenoids.spanA = Number(setPoints.solenoids.spanA);
    }
    if (setPoints.solenoids.spanB !== undefined) {
      hmPackage.ch1Solenoids.spanB = Number(setPoints.solenoids.spanB);
    }
    if (setPoints.solenoids.purge !== undefined) {
      hmPackage.ch1Solenoids.purge = Number(setPoints.solenoids.purge);
    }
    if (setPoints.solenoids.zero !== undefined) {
      hmPackage.ch1Solenoids.zero = Number(setPoints.solenoids.zero);
    }
    if (setPoints.solenoids.vacuum !== undefined) {
      hmPackage.ch1Solenoids.vacuum = Number(setPoints.solenoids.vacuum);
    }
    if (setPoints.solenoids.test !== undefined) {
      hmPackage.ch1Solenoids.test = Number(setPoints.solenoids.test);
    }
  }

  if (setPoints.lhcGlowPlugAndHeadHeaterStatus !== undefined) {
    if (setPoints.lhcGlowPlugAndHeadHeaterStatus.headHeater !== undefined) {
      hmPackage.lhcGlowPlugAndHeadHeaterStatus.headHeater1 = Number(
        setPoints.lhcGlowPlugAndHeadHeaterStatus.headHeater,
      );
    }
    if (setPoints.lhcGlowPlugAndHeadHeaterStatus.glowPlug !== undefined) {
      hmPackage.lhcGlowPlugAndHeadHeaterStatus.glowPlug1 = Number(
        setPoints.lhcGlowPlugAndHeadHeaterStatus.glowPlug,
      );
    }
    if (setPoints.lhcGlowPlugAndHeadHeaterStatus.lhc !== undefined) {
      hmPackage.lhcGlowPlugAndHeadHeaterStatus.lhc1 = Number(
        setPoints.lhcGlowPlugAndHeadHeaterStatus.lhc,
      );
    }
  }

  if (setPoints.userDigitalOutput !== undefined) {
    if (setPoints.userDigitalOutput.isolated0 !== undefined) {
      hmPackage.userDigitalOutput.isolated0 =
        setPoints.userDigitalOutput.isolated0;
    }
    if (setPoints.userDigitalOutput.isolated1 !== undefined) {
      hmPackage.userDigitalOutput.isolated1 =
        setPoints.userDigitalOutput.isolated1;
    }
    if (setPoints.userDigitalOutput.isolated2 !== undefined) {
      hmPackage.userDigitalOutput.isolated2 =
        setPoints.userDigitalOutput.isolated2;
    }
    if (setPoints.userDigitalOutput.isolated3 !== undefined) {
      hmPackage.userDigitalOutput.isolated3 =
        setPoints.userDigitalOutput.isolated3;
    }

    if (setPoints.userDigitalOutput.nonIsolated0 !== undefined) {
      hmPackage.userDigitalOutput.nonIsolated0 =
        setPoints.userDigitalOutput.nonIsolated0;
    }
    if (setPoints.userDigitalOutput.nonIsolated1 !== undefined) {
      hmPackage.userDigitalOutput.nonIsolated1 =
        setPoints.userDigitalOutput.nonIsolated1;
    }
    if (setPoints.userDigitalOutput.nonIsolated2 !== undefined) {
      hmPackage.userDigitalOutput.nonIsolated2 =
        setPoints.userDigitalOutput.nonIsolated2;
    }
    if (setPoints.userDigitalOutput.nonIsolated3 !== undefined) {
      hmPackage.userDigitalOutput.nonIsolated3 =
        setPoints.userDigitalOutput.nonIsolated3;
    }
  }

  if (setPoints.fidPressureSetpointMilliBar !== undefined) {
    hmPackage.ch1FidPressureSetpoint = Math.round(
      setPoints.fidPressureSetpointMilliBar / coef.fidPressureSetpointMilliBar,
    );
  }

  if (setPoints.cpPressureSetpointMilliBar !== undefined) {
    hmPackage.ch1CpPressureSetpoint = Math.round(
      setPoints.cpPressureSetpointMilliBar / coef.cpPressureSetpointMilliBar,
    );
  }

  if (setPoints.pumpRpmSetpoint !== undefined) {
    hmPackage.pumpRpmSetpoint = setPoints.pumpRpmSetpoint;
  }

  if (setPoints.fuelPressureSetpointBar !== undefined) {
    hmPackage.ch1FuelPressureSetpoint = Math.round(
      setPoints.fuelPressureSetpointBar / coef.fuelPressureSetpointBar,
    );
  }

  if (setPoints.airPressureSetpointBar !== undefined) {
    hmPackage.ch1AirPressureSetpoint = Math.round(
      setPoints.airPressureSetpointBar / coef.airPressureSetpointBar,
    );
  }

  if (setPoints.glowplugCurrentAmpere !== undefined) {
    hmPackage.glowplugCurrent = Math.round(
      setPoints.glowplugCurrentAmpere / coef.glowplugCurrentAmpere,
    );
  }

  if (setPoints.probeTemperatureSetpointDegC !== undefined) {
    hmPackage.ch1ProbeTemperatureSetpoint = Math.round(
      setPoints.probeTemperatureSetpointDegC /
        coef.probeTemperatureSetpointDegC,
    );
  }

  if (setPoints.headTemperatureSetpointDegC !== undefined) {
    hmPackage.ch1HeadTemperatureSetpoint = Math.round(
      setPoints.headTemperatureSetpointDegC / coef.headTemperatureSetpointDegC,
    );
  }

  if (setPoints.span !== undefined) {
    hmPackage.span1 = setPoints.span;
  }

  if (setPoints.zero !== undefined) {
    hmPackage.zero1 = setPoints.zero;
  }

  return hmPackage;
};

export const setPointsUpdate = (
  setPoints: SetPoints,
  hmPackage: ReadonlyDeep<HmPackage>,
  coef: ReadonlyDeep<SetPointsCoef> = setPointsCoefInitial,
): SetPoints => {
  setPoints.solenoids.fuel = Boolean(hmPackage.ch1Solenoids.fuel);
  setPoints.solenoids.air = Boolean(hmPackage.ch1Solenoids.air);
  setPoints.solenoids.spanA = Boolean(hmPackage.ch1Solenoids.spanA);
  setPoints.solenoids.spanB = Boolean(hmPackage.ch1Solenoids.spanB);
  setPoints.solenoids.purge = Boolean(hmPackage.ch1Solenoids.purge);
  setPoints.solenoids.zero = Boolean(hmPackage.ch1Solenoids.zero);
  setPoints.solenoids.vacuum = Boolean(hmPackage.ch1Solenoids.vacuum);
  setPoints.solenoids.test = Boolean(hmPackage.ch1Solenoids.test);

  setPoints.lhcGlowPlugAndHeadHeaterStatus.headHeater = Boolean(
    hmPackage.lhcGlowPlugAndHeadHeaterStatus.headHeater1,
  );
  setPoints.lhcGlowPlugAndHeadHeaterStatus.glowPlug = Boolean(
    hmPackage.lhcGlowPlugAndHeadHeaterStatus.glowPlug1,
  );
  setPoints.lhcGlowPlugAndHeadHeaterStatus.lhc = Boolean(
    hmPackage.lhcGlowPlugAndHeadHeaterStatus.lhc1,
  );

  setPoints.userDigitalOutput.isolated0 = hmPackage.userDigitalOutput.isolated0;
  setPoints.userDigitalOutput.isolated1 = hmPackage.userDigitalOutput.isolated1;
  setPoints.userDigitalOutput.isolated2 = hmPackage.userDigitalOutput.isolated2;
  setPoints.userDigitalOutput.isolated3 = hmPackage.userDigitalOutput.isolated3;

  setPoints.userDigitalOutput.nonIsolated0 =
    hmPackage.userDigitalOutput.nonIsolated0;
  setPoints.userDigitalOutput.nonIsolated1 =
    hmPackage.userDigitalOutput.nonIsolated1;
  setPoints.userDigitalOutput.nonIsolated2 =
    hmPackage.userDigitalOutput.nonIsolated2;
  setPoints.userDigitalOutput.nonIsolated3 =
    hmPackage.userDigitalOutput.nonIsolated3;

  setPoints.fidPressureSetpointMilliBar =
    hmPackage.ch1FidPressureSetpoint * coef.fidPressureSetpointMilliBar;

  setPoints.cpPressureSetpointMilliBar =
    hmPackage.ch1CpPressureSetpoint * coef.cpPressureSetpointMilliBar;

  setPoints.pumpRpmSetpoint = hmPackage.pumpRpmSetpoint;

  setPoints.fuelPressureSetpointBar =
    hmPackage.ch1FuelPressureSetpoint * coef.fuelPressureSetpointBar;

  setPoints.airPressureSetpointBar =
    hmPackage.ch1AirPressureSetpoint * coef.airPressureSetpointBar;

  setPoints.glowplugCurrentAmpere =
    hmPackage.glowplugCurrent * coef.glowplugCurrentAmpere;

  setPoints.probeTemperatureSetpointDegC =
    hmPackage.ch1ProbeTemperatureSetpoint * coef.probeTemperatureSetpointDegC;

  setPoints.headTemperatureSetpointDegC =
    hmPackage.ch1HeadTemperatureSetpoint * coef.headTemperatureSetpointDegC;

  setPoints.span = hmPackage.span1;

  setPoints.zero = hmPackage.zero1;

  return setPoints;
};
