// Hey Emacs, this is -*- coding: utf-8 -*-

export type DeepWriteable<T> = {
  -readonly [P in keyof T]: DeepWriteable<T[P]>;
};
