// Hey Emacs, this is -*- coding: utf-8 -*-

export const castle = {
  httpProtocol: 'http:',
  httpHost: 'localhost',
  httpPort: 4000,
  httpPath: '/graphql',
  wsProtocol: 'ws:',
  wsHost: 'localhost',
  wsPort: 5000,
  wsPath: '/graphql',
  // wsClientHost can be different from wsHost.
  // If wsClientHost ie equal "localhost", the client is going to use its
  // window.location.hostname to find its ws address.
  // Otherwise, the client will use wsClientHost value as it is.
  // wsClientHost: 'localhost',
  wsClientHost: 's50-host-0',
};
