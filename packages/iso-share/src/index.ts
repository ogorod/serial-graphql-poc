// Hey Emacs, this is -*- coding: utf-8 -*-

export * as config from '~/config';

export const inBrowser = (): boolean => {
  return typeof window !== 'undefined';
};

export interface WebSocketConnectionParams {
  // The same meaning as in HTTP authorization header
  authorization: string;
}
