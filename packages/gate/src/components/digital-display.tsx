// Hey Emacs, this is -*- coding: utf-8 -*-

import PropTypes from 'prop-types';

import { Field, Label, InputGroup } from '@zendeskgarden/react-forms';

import {
  DigitalDisplayUnit,
  DigitalDisplayValue,
} from '~/components/styled/digital-display';

export const propTypes = {
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.number,
  unit: PropTypes.string.isRequired,
};

export type Props = PropTypes.InferProps<typeof propTypes>;

export const DigitalDisplay = ({
  id,
  label,
  value,
  unit,
}: Props): JSX.Element => {
  const fieldId = id ? `${id}-digital-display-field` : undefined;

  return (
    <Field id={fieldId}>
      <Label>{label}</Label>
      <InputGroup>
        <DigitalDisplayValue value={value?.toString() ?? NaN.toString()} />
        <DigitalDisplayUnit value={unit} />
      </InputGroup>
    </Field>
  );
};
