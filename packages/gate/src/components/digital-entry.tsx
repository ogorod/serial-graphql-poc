// Hey Emacs, this is -*- coding: utf-8 -*-

import type { ChangeEventHandler } from 'react';

import PropTypes from 'prop-types';

import { Field, Label, InputGroup, Message } from '@zendeskgarden/react-forms';
import { Button } from '@zendeskgarden/react-buttons';

import { ButtonsRow } from '~/components/styled/grid';

import { Unit, Value, Spacer } from '~/components/styled/digital-entry';

const propTypes = {
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  message: PropTypes.string,
  isValid: PropTypes.bool.isRequired,
  wasEdited: PropTypes.bool.isRequired,
  isReadOnly: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  onOk: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

type PropsInferred = PropTypes.InferProps<typeof propTypes>;

interface Props extends PropsInferred {
  onChange: ChangeEventHandler<HTMLInputElement>;
  onOk: () => void;
  onCancel: () => void;
}

export const DigitalEntry = ({
  id,
  label,
  value,
  unit,
  message,
  isValid,
  wasEdited,
  isReadOnly,
  onChange,
  onOk,
  onCancel,
}: Props): JSX.Element => {
  const digitalEntryId = id ? `${id}-digital-entry` : undefined;

  return (
    <Field id={digitalEntryId}>
      <Label>{label}</Label>
      <InputGroup>
        <Value
          value={value}
          readOnly={isReadOnly}
          onChange={onChange}
          onKeyDown={(event): void => {
            switch (event.key) {
              case 'Enter':
                onOk();
                break;
              case 'Escape':
                onCancel();
                break;
              default:
            }
          }}
          validation={isValid ? undefined : 'error'}
        />
        <Unit value={unit} />
      </InputGroup>
      {wasEdited || message ? (
        <ButtonsRow>
          <Button
            size="small"
            type="button"
            disabled={!isValid}
            isPrimary
            onClick={onOk}
          >
            Ok
          </Button>
          <Button size="small" type="button" onClick={onCancel}>
            Cancel
          </Button>
          <Spacer />
          {message ? (
            <Message validation={isValid ? undefined : 'error'}>
              {message}
            </Message>
          ) : undefined}
        </ButtonsRow>
      ) : undefined}
    </Field>
  );
};
