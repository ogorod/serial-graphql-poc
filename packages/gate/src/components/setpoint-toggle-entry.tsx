// Hey Emacs, this is -*- coding: utf-8 -*-

import { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { ToggleEntry } from '~/components/toggle-entry';

const propTypes = {
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  receivedSetpoint: PropTypes.bool,
  sendSetpoint: PropTypes.func.isRequired,
};

type PropsInferred = PropTypes.InferProps<typeof propTypes>;

interface Props extends PropsInferred {
  sendSetpoint: (value: boolean) => void;
}

export const SetpointToggleEntry = ({
  id,
  label,
  receivedSetpoint,
  sendSetpoint,
}: Props): JSX.Element => {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setChecked((prevState) => receivedSetpoint ?? prevState);
  }, [receivedSetpoint]);

  return (
    <ToggleEntry
      id={id}
      label={label}
      checked={checked}
      onChange={(_event): void => {
        sendSetpoint(!checked);
      }}
    />
  );
};
