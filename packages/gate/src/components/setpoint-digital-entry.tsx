// Hey Emacs, this is -*- coding: utf-8 -*-

import PropTypes from 'prop-types';

import { useState, useEffect, useCallback } from 'react';

// import type { StateSetpointNumber } from '~/lib/iso/set-points';
import { stateSetpointNumberInitial } from '~/lib/iso/setpoints';

import { DigitalEntry } from '~/components/digital-entry';

const propTypes = {
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  receivedSetpoint: PropTypes.number,
  sendSetpoint: PropTypes.func.isRequired,
  analyserConnected: PropTypes.bool.isRequired,
};

type PropsInferred = PropTypes.InferProps<typeof propTypes>;

interface Props extends PropsInferred {
  sendSetpoint: (value: number) => void;
}

export const SetpointDigitalEntry = ({
  id,
  label,
  unit,
  receivedSetpoint,
  sendSetpoint,
  analyserConnected,
}: Props): JSX.Element => {
  const [state, setState] = useState(stateSetpointNumberInitial);

  useEffect(() => {
    setState((prevState) => {
      const setPointValue = receivedSetpoint ?? prevState.value;
      const setPointCurrent = receivedSetpoint?.toString() ?? prevState.input;

      return {
        ...prevState,
        value: setPointValue,
        current: setPointCurrent,
        input: setPointCurrent,
      };
    });
  }, [receivedSetpoint]);

  const updateSetPoint = useCallback((): void => {
    if (!state.inputChanged) {
      return;
    }

    const setPoint = Number(state.input || NaN);

    if (!Number.isInteger(setPoint)) {
      return;
    }

    sendSetpoint(setPoint);

    // TODO: Ideally, we should await until setPumpRpm promise resolves
    //       and indicate error if server errors or does not respond.
    setState((prevState) => ({
      ...prevState,
      inputChanged: false,
    }));
  }, [sendSetpoint, state.inputChanged, state.input]);

  const cancelSetPointInput = useCallback((): void => {
    setState((prevState) => {
      return {
        ...prevState,
        input: prevState.current,
        inputChanged: false,
      };
    });
  }, [setState]);

  return (
    <DigitalEntry
      id={id}
      label={label}
      value={state.input}
      unit={unit}
      wasEdited={state.inputChanged}
      isReadOnly={!analyserConnected}
      onChange={(event): void => {
        setState((prevState) => ({
          ...prevState,
          input: event.target.value,
          inputChanged: true,
        }));
      }}
      onOk={updateSetPoint}
      onCancel={cancelSetPointInput}
      isValid={((): boolean => {
        const inputNumber = Number(state.input || NaN);

        if (!Number.isInteger(inputNumber)) {
          return false;
        }

        return true;
      })()}
      message={((): string | undefined => {
        const { input } = state;

        // Do not show error message on initial input (NaN)
        if (input === stateSetpointNumberInitial.input) {
          return undefined;
        }

        const inputNumber = Number(input || NaN);

        if (!Number.isInteger(inputNumber)) {
          return 'Should be an integer';
        }

        return undefined;
      })()}
    />
  );
};
