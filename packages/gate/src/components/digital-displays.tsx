// Hey Emacs, this is -*- coding: utf-8 -*-

import { useFid50Context } from '~/lib/front/fid50-context';

import { DigitalDisplay } from '~/components/digital-display';

export const MainPcbUpTimeSecondDigitalDisplay = (): JSX.Element => {
  const { fid50Query } = useFid50Context();

  return (
    <DigitalDisplay
      id="main-pcb-up-time-second"
      label="Main PCB Up-time"
      value={fid50Query?.fid50State.mainPcbUpTimeSecond}
      unit="sec"
    />
  );
};

export const PumpPcbUpTimeSecondDigitalDisplay = (): JSX.Element => {
  const { fid50Query } = useFid50Context();

  return (
    <DigitalDisplay
      id="pump-pcb-up-time-second"
      label="Pump PCB Up-time"
      value={fid50Query?.fid50State.pumpPcbUpTimeSecond}
      unit="sec"
    />
  );
};

export const PumpRpmDigitalDisplay = (): JSX.Element => {
  const { fid50Query } = useFid50Context();

  return (
    <DigitalDisplay
      id="pump-rpm"
      label="Pump RPM"
      value={fid50Query?.fid50State.pumpRpm}
      unit="RPM"
    />
  );
};
