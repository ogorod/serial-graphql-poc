// Hey Emacs, this is -*- coding: utf-8 -*-

import styled from 'styled-components';

export const Root = styled.div`
  // padding: 0 0.5rem;
  padding: 0.5rem;
  display: flex;
  flex-direction: column;
  // justify-content: center;
  // align-items: center;
  min-height: 100vh;
  height: 100vh;
`;

export const Main = styled.main`
  flex-grow: 1;
  flex-shrink: 1;
`;
