// Hey Emacs, this is -*- coding: utf-8 -*-

import styled from 'styled-components';

import { Well } from '@zendeskgarden/react-notifications';

export const SpacedWell = styled(Well)`
  // background: lightblue;
  // display: flex;
  // column-gap: 20px;
  & > *:not(:last-child) {
    // background: lightgreen;
    margin-bottom: ${(props): string => props.theme.space.sm};
  }
`;
