// Hey Emacs, this is -*- coding: utf-8 -*-

import styled from 'styled-components';

import { MD, XL, XXL } from '@zendeskgarden/react-typography';

// TODO: Write to zendeskgarden issues to fix types export upstream.
import type { MD as MDType, XXL as XXLType } from '~/types/zendeskgarden-patch';

export const H1 = styled(XXL as XXLType).attrs({
  isBold: true,
  tag: 'h1',
})`
  margin-bottom: ${(props): string => props.theme.fontSizes.sm};
  color: ${(props): string => props.theme.colors.foreground};
`;

export const H2 = styled(XL).attrs({
  isBold: true,
  tag: 'h2',
})`
  margin-bottom: ${(props): string => props.theme.fontSizes.sm};
  color: ${(props): string => props.theme.colors.foreground};
`;

export const P = styled(MD as MDType)`
  margin-top: ${(props): string => props.theme.space.xxs};
`;
