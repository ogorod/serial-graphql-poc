// Hey Emacs, this is -*- coding: utf-8 -*-

import styled from 'styled-components';

import { Input } from '@zendeskgarden/react-forms';

export const DigitalDisplayUnit = styled(Input).attrs({
  isCompact: true,
  readOnly: true,
})`
  max-width: 6em;
  text-align: center;
  // &[readonly] {
  //   background-color: lightblue;
  // }
`;

export const DigitalDisplayValue = styled(Input).attrs({
  isCompact: true,
  readOnly: true,
})``;
