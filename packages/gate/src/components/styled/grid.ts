// Hey Emacs, this is -*- coding: utf-8 -*-

import styled from 'styled-components';

import { Row } from '@zendeskgarden/react-grid';

export const ButtonsRow = styled(Row)`
  // background: pink;
  margin-left: 0;
  margin-right: 0;
  margin-top: ${(props): string => props.theme.space.xs};
  margin-bottom: ${(props): string => props.theme.space.xs};
  gap: ${(props): string => props.theme.space.xs};
`;
