// Hey Emacs, this is -*- coding: utf-8 -*-

import styled from 'styled-components';

import { Input } from '@zendeskgarden/react-forms';

export const Unit = styled(Input).attrs({
  isCompact: true,
  readOnly: true,
})`
  max-width: 6em;
  text-align: center;
  // &[readonly] {
  //   background-color: lightblue;
  // }
`;

export const Value = styled(Input).attrs({
  isCompact: true,
  autoComplete: 'off',
  autoCorrect: 'off',
  autoCapitalize: 'off',
  spellCheck: 'false',
})``;

export const Spacer = styled.div`
  flex-grow: 1;
`;
