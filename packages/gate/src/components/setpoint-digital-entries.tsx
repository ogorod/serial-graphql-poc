// Hey Emacs, this is -*- coding: utf-8 -*-

import { useMutation } from '@apollo/client';

import { useFid50Context } from '~/lib/front/fid50-context';

import {
  Fid50Document,
  SetPumpRpmSetpointDocument,
} from '~/lib/front/graphql-operations';

import { SetpointDigitalEntry } from '~/components/setpoint-digital-entry';

export const PumpRpmSetpointDigitalEntry = (): JSX.Element => {
  const [setPumpRpmSetpoint] = useMutation(SetPumpRpmSetpointDocument);

  const { fid50Query } = useFid50Context();

  return (
    <SetpointDigitalEntry
      id="pump-rpm-setpoint"
      label="Pump RPM Setpoint"
      unit="RPM"
      receivedSetpoint={fid50Query?.fid50SetPoints.pumpRpmSetpoint}
      sendSetpoint={(value): void => {
        setPumpRpmSetpoint({
          variables: { value },
          refetchQueries: [Fid50Document],
        });
      }}
      analyserConnected={fid50Query !== undefined}
    />
  );
};
