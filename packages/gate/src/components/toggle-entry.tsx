// Hey Emacs, this is -*- coding: utf-8 -*-

import type { ChangeEventHandler } from 'react';

import PropTypes from 'prop-types';

import { Field, Label, Toggle } from '@zendeskgarden/react-forms';

const propTypes = {
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

type PropsInferred = PropTypes.InferProps<typeof propTypes>;

interface Props extends PropsInferred {
  onChange: ChangeEventHandler<HTMLInputElement>;
}

export const ToggleEntry = ({
  id,
  label,
  checked,
  onChange,
}: Props): JSX.Element => {
  const toggleEntryId = id ? `${id}-toggle-entry` : undefined;

  return (
    <Field id={toggleEntryId}>
      <Toggle checked={checked} onChange={onChange}>
        <Label>{label}</Label>
      </Toggle>
    </Field>
  );
};
