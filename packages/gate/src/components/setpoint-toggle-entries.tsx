// Hey Emacs, this is -*- coding: utf-8 -*-

import { useMutation } from '@apollo/client';

import { useFid50Context } from '~/lib/front/fid50-context';

import {
  Fid50Document,
  SetSolenoidsVacuumDocument,
} from '~/lib/front/graphql-operations';

import { SetpointToggleEntry } from '~/components/setpoint-toggle-entry';

export const SolenoidsVacuumToggleEntry = (): JSX.Element => {
  const [setSolenoidsVacuum] = useMutation(SetSolenoidsVacuumDocument);

  const { fid50Query } = useFid50Context();

  return (
    <SetpointToggleEntry
      id="solenoids-vacuum"
      label="Vacuum"
      receivedSetpoint={fid50Query?.fid50SetPoints.solenoids.vacuum}
      sendSetpoint={(value): void => {
        setSolenoidsVacuum({
          variables: { value },
          refetchQueries: [Fid50Document],
        });
      }}
    />
  );
};
