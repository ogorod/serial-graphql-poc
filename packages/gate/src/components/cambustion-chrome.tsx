// Hey Emacs, this is -*- coding: utf-8 -*-

import PropTypes from 'prop-types';

import {
  Chrome,
  // Body,
  // Content,
  // Main,
  // Header,
  // HeaderItem,
  // HeaderItemIcon,
  // HeaderItemText,
  // Footer,
  // FooterItem,
  // Nav,
  // NavItem,
  // NavItemIcon,
  // NavItemText,
  // Sidebar,
  // SkipNav,
} from '@zendeskgarden/react-chrome';

const propTypes = {
  body: PropTypes.any.isRequired,
};

type Props = PropTypes.InferProps<typeof propTypes>;

export const CambustionChrome = ({ body }: Props): JSX.Element => {
  return <Chrome>{body}</Chrome>;
};
