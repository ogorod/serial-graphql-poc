// Hey Emacs, this is -*- coding: utf-8 -*-

export interface StateSetpointNumber {
  value: number;
  current: string;
  input: string;
  inputChanged: boolean;
}

export const stateSetpointNumberInitial: StateSetpointNumber = {
  value: NaN,
  current: NaN.toString(),
  input: NaN.toString(),
  inputChanged: false,
} as const;
