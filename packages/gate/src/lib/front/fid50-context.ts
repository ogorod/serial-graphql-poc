// Hey Emacs, this is -*- coding: utf-8 -*-

import { createContext, useContext } from 'react';

import type { Fid50Query } from '~/lib/front/graphql-operations';

export interface Fid50ContextValue {
  fid50Query?: Fid50Query;
}

export const Fid50Context = createContext<Fid50ContextValue>({});

export const useFid50Context = (): Fid50ContextValue =>
  useContext(Fid50Context);
