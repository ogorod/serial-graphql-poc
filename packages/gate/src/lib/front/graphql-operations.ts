/* eslint-disable */
// This file is generated with graphql-codegen. see ./codegen.yaml
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type CastleCounter = {
  __typename?: 'CastleCounter';
  count: Scalars['Float'];
};

export type Fid50ExhaustTcFaults = {
  __typename?: 'Fid50ExhaustTcFaults';
  exhaust: Scalars['Boolean'];
  safeState: Scalars['Boolean'];
};

export type Fid50GainRangeAndHtStatus = {
  __typename?: 'Fid50GainRangeAndHtStatus';
  amplificationStage: Scalars['Float'];
  ht: Scalars['Boolean'];
};

export type Fid50GainRangeAndHtStatusInput = {
  amplificationStage: InputMaybe<Scalars['Float']>;
  ht: InputMaybe<Scalars['Boolean']>;
};

export type Fid50GasPressureFaults = {
  __typename?: 'Fid50GasPressureFaults';
  air: Scalars['Boolean'];
  fuel: Scalars['Boolean'];
  purge: Scalars['Boolean'];
  spanA: Scalars['Boolean'];
  spanB: Scalars['Boolean'];
  vacuum: Scalars['Boolean'];
  zero: Scalars['Boolean'];
};

export type Fid50LhcGlowPlugAndHeadHeaterStatus = {
  __typename?: 'Fid50LhcGlowPlugAndHeadHeaterStatus';
  glowPlug: Scalars['Boolean'];
  headHeater: Scalars['Boolean'];
  lhc: Scalars['Boolean'];
};

export type Fid50LhcGlowPlugAndHeadHeaterStatusInput = {
  glowPlug: InputMaybe<Scalars['Boolean']>;
  headHeater: InputMaybe<Scalars['Boolean']>;
  lhc: InputMaybe<Scalars['Boolean']>;
};

export type Fid50ProbeAndHeadTcFaults = {
  __typename?: 'Fid50ProbeAndHeadTcFaults';
  head: Scalars['Boolean'];
  probe: Scalars['Boolean'];
};

export type Fid50RxCh1SolenoidsFaults = {
  __typename?: 'Fid50RxCh1SolenoidsFaults';
  air: Scalars['Float'];
  fuel: Scalars['Float'];
  purge: Scalars['Float'];
  spanA: Scalars['Float'];
  spanB: Scalars['Float'];
  test: Scalars['Float'];
  vacuum: Scalars['Float'];
  zero: Scalars['Float'];
};

export type Fid50RxCh2SolenoidsFaults = {
  __typename?: 'Fid50RxCh2SolenoidsFaults';
  air: Scalars['Float'];
  fuel: Scalars['Float'];
  purge: Scalars['Float'];
  spanA: Scalars['Float'];
  spanB: Scalars['Float'];
  test: Scalars['Float'];
  vacuum: Scalars['Float'];
  zero: Scalars['Float'];
};

export type Fid50RxExhaustTcFaults = {
  __typename?: 'Fid50RxExhaustTcFaults';
  exhaust1: Scalars['Float'];
  exhaust2: Scalars['Float'];
  safeState: Scalars['Float'];
};

export type Fid50RxGasPressureFaults = {
  __typename?: 'Fid50RxGasPressureFaults';
  air: Scalars['Float'];
  fuel: Scalars['Float'];
  purge: Scalars['Float'];
  spanA: Scalars['Float'];
  spanB: Scalars['Float'];
  vacuum: Scalars['Float'];
  zero: Scalars['Float'];
};

export type Fid50RxPackage = {
  __typename?: 'Fid50RxPackage';
  ambientTemperature: Scalars['Float'];
  ch1AirBleed: Scalars['Float'];
  ch1AirPressure: Scalars['Float'];
  ch1CpBleed: Scalars['Float'];
  ch1CpPressure: Scalars['Float'];
  ch1ExhaustTemperature: Scalars['Float'];
  ch1FidBleed: Scalars['Float'];
  ch1FidPressure: Scalars['Float'];
  ch1FuelBleed: Scalars['Float'];
  ch1FuelPressure: Scalars['Float'];
  ch1GlowPlugCurrent: Scalars['Float'];
  ch1HeadDrivePercent: Scalars['Float'];
  ch1HeadTemperature: Scalars['Float'];
  ch1OutputPpm: Scalars['Float'];
  ch1ProbeDrivePercent: Scalars['Float'];
  ch1ProbeTemperature: Scalars['Float'];
  ch1SolenoidsFaults: Fid50RxCh1SolenoidsFaults;
  ch2CpBleed: Scalars['Float'];
  ch2ExhaustTemperature: Scalars['Float'];
  ch2FidBleed: Scalars['Float'];
  ch2GlowPlugCurrent: Scalars['Float'];
  ch2HeadTemperature: Scalars['Float'];
  ch2OutputV: Scalars['Float'];
  ch2ProbeDrivePercent: Scalars['Float'];
  ch2ProbeTemperature: Scalars['Float'];
  ch2SolenoidsFaults: Fid50RxCh2SolenoidsFaults;
  exhaustTcFaults: Fid50RxExhaustTcFaults;
  fanRpm: Scalars['Float'];
  firmwareVersionMajor: Scalars['Float'];
  firmwareVersionMinor: Scalars['Float'];
  gasPressureFaults: Fid50RxGasPressureFaults;
  mainPcbUpTimeSecond: Scalars['Float'];
  mainsFrequency: Scalars['Float'];
  maxFactor: Scalars['Float'];
  outputPpm: Scalars['Float'];
  probeAndHeadTcFaults: Fid50RxProbeAndHeadTcFaults;
  pumpPcbUpTimeSecond: Scalars['Float'];
  pumpRpm: Scalars['Float'];
  pumpTotalRunningTimeMinute: Scalars['Float'];
  userDigitalInputs: Fid50RxUserDigitalInputs;
  zeroOffset: Scalars['Float'];
};

export type Fid50RxProbeAndHeadTcFaults = {
  __typename?: 'Fid50RxProbeAndHeadTcFaults';
  head1: Scalars['Float'];
  head2: Scalars['Float'];
  probe1: Scalars['Float'];
  probe2: Scalars['Float'];
};

export type Fid50RxUserDigitalInputs = {
  __typename?: 'Fid50RxUserDigitalInputs';
  isolated0: Scalars['Float'];
  isolated1: Scalars['Float'];
  isolated2: Scalars['Float'];
  isolated3: Scalars['Float'];
  isolatedValue: Scalars['Float'];
  nonIsolated0: Scalars['Float'];
  nonIsolated1: Scalars['Float'];
  nonIsolated2: Scalars['Float'];
  nonIsolated3: Scalars['Float'];
  nonIsolatedValue: Scalars['Float'];
};

export type Fid50SetPoints = {
  __typename?: 'Fid50SetPoints';
  airPressureSetpointBar: Scalars['Float'];
  cpPressureSetpointMilliBar: Scalars['Float'];
  fidPressureSetpointMilliBar: Scalars['Float'];
  fuelPressureSetpointBar: Scalars['Float'];
  gainRangeAndHtStatus: Fid50GainRangeAndHtStatus;
  glowplugCurrentAmpere: Scalars['Float'];
  headTemperatureSetpointDegC: Scalars['Float'];
  lhcGlowPlugAndHeadHeaterStatus: Fid50LhcGlowPlugAndHeadHeaterStatus;
  probeTemperatureSetpointDegC: Scalars['Float'];
  pumpRpmSetpoint: Scalars['Float'];
  solenoids: Fid50Solenoids;
  span: Scalars['Float'];
  userDigitalOutput: Fid50UserDigitalOutput;
  zero: Scalars['Float'];
};

export type Fid50SetPointsInput = {
  airPressureSetpointBar: InputMaybe<Scalars['Float']>;
  cpPressureSetpointMilliBar: InputMaybe<Scalars['Float']>;
  fidPressureSetpointMilliBar: InputMaybe<Scalars['Float']>;
  fuelPressureSetpointBar: InputMaybe<Scalars['Float']>;
  gainRangeAndHtStatus: InputMaybe<Fid50GainRangeAndHtStatusInput>;
  glowplugCurrentAmpere: InputMaybe<Scalars['Float']>;
  headTemperatureSetpointDegC: InputMaybe<Scalars['Float']>;
  lhcGlowPlugAndHeadHeaterStatus: InputMaybe<Fid50LhcGlowPlugAndHeadHeaterStatusInput>;
  probeTemperatureSetpointDegC: InputMaybe<Scalars['Float']>;
  pumpRpmSetpoint: InputMaybe<Scalars['Float']>;
  solenoids: InputMaybe<Fid50SolenoidsInput>;
  span: InputMaybe<Scalars['Float']>;
  userDigitalOutput: InputMaybe<Fid50UserDigitalOutputInput>;
  zero: InputMaybe<Scalars['Float']>;
};

export type Fid50Solenoids = {
  __typename?: 'Fid50Solenoids';
  air: Scalars['Boolean'];
  fuel: Scalars['Boolean'];
  purge: Scalars['Boolean'];
  spanA: Scalars['Boolean'];
  spanB: Scalars['Boolean'];
  test: Scalars['Boolean'];
  vacuum: Scalars['Boolean'];
  zero: Scalars['Boolean'];
};

export type Fid50SolenoidsFaults = {
  __typename?: 'Fid50SolenoidsFaults';
  air: Scalars['Boolean'];
  fuel: Scalars['Boolean'];
  purge: Scalars['Boolean'];
  spanA: Scalars['Boolean'];
  spanB: Scalars['Boolean'];
  test: Scalars['Boolean'];
  vacuum: Scalars['Boolean'];
  zero: Scalars['Boolean'];
};

export type Fid50SolenoidsInput = {
  air: InputMaybe<Scalars['Boolean']>;
  fuel: InputMaybe<Scalars['Boolean']>;
  purge: InputMaybe<Scalars['Boolean']>;
  spanA: InputMaybe<Scalars['Boolean']>;
  spanB: InputMaybe<Scalars['Boolean']>;
  test: InputMaybe<Scalars['Boolean']>;
  vacuum: InputMaybe<Scalars['Boolean']>;
  zero: InputMaybe<Scalars['Boolean']>;
};

export type Fid50State = {
  __typename?: 'Fid50State';
  airBleedPercent: Scalars['Float'];
  airPressureBar: Scalars['Float'];
  ambientTemperatureDegC: Scalars['Float'];
  cpBleedPercent: Scalars['Float'];
  cpPressureMilliBar: Scalars['Float'];
  exhaustTcFaults: Fid50ExhaustTcFaults;
  exhaustTemperatureDegC: Scalars['Float'];
  fanRpm: Scalars['Float'];
  fidBleedPercent: Scalars['Float'];
  fidPressureMilliBar: Scalars['Float'];
  firmwareVersionMajor: Scalars['Float'];
  firmwareVersionMinor: Scalars['Float'];
  fuelBleedPercent: Scalars['Float'];
  fuelPressureBar: Scalars['Float'];
  gasPressureFaults: Fid50GasPressureFaults;
  glowPlugCurrentAmpere: Scalars['Float'];
  headDrivePercent: Scalars['Float'];
  headTemperatureDegC: Scalars['Float'];
  mainPcbUpTimeSecond: Scalars['Float'];
  mainsFrequencyHz: Scalars['Float'];
  outputPpm: Scalars['Float'];
  probeAndHeadTcFaults: Fid50ProbeAndHeadTcFaults;
  probeDrivePercent: Scalars['Float'];
  probeTemperatureDegC: Scalars['Float'];
  pumpPcbUpTimeSecond: Scalars['Float'];
  pumpRpm: Scalars['Float'];
  pumpTotalRunningTimeMinute: Scalars['Float'];
  solenoidsFaults: Fid50SolenoidsFaults;
  userDigitalInputs: Fid50UserDigitalInputs;
};

export type Fid50TxChNSolenoids = {
  __typename?: 'Fid50TxChNSolenoids';
  air: Scalars['Float'];
  fuel: Scalars['Float'];
  purge: Scalars['Float'];
  spanA: Scalars['Float'];
  spanB: Scalars['Float'];
  test: Scalars['Float'];
  vacuum: Scalars['Float'];
  zero: Scalars['Float'];
};

export type Fid50TxChNSolenoidsInput = {
  air: InputMaybe<Scalars['Float']>;
  fuel: InputMaybe<Scalars['Float']>;
  purge: InputMaybe<Scalars['Float']>;
  spanA: InputMaybe<Scalars['Float']>;
  spanB: InputMaybe<Scalars['Float']>;
  test: InputMaybe<Scalars['Float']>;
  vacuum: InputMaybe<Scalars['Float']>;
  zero: InputMaybe<Scalars['Float']>;
};

export type Fid50TxGainRangeAndHtStatus = {
  __typename?: 'Fid50TxGainRangeAndHtStatus';
  ch1AmplificationStage: Scalars['Float'];
  ch2AmplificationStage: Scalars['Float'];
  ht1: Scalars['Float'];
  ht2: Scalars['Float'];
};

export type Fid50TxGainRangeAndHtStatusInput = {
  ch1AmplificationStage: InputMaybe<Scalars['Float']>;
  ch2AmplificationStage: InputMaybe<Scalars['Float']>;
  ht1: InputMaybe<Scalars['Float']>;
  ht2: InputMaybe<Scalars['Float']>;
};

export type Fid50TxLhcGlowPlugAndHeadHeaterStatus = {
  __typename?: 'Fid50TxLhcGlowPlugAndHeadHeaterStatus';
  glowPlug1: Scalars['Float'];
  glowPlug2: Scalars['Float'];
  headHeater1: Scalars['Float'];
  headHeater2: Scalars['Float'];
  lhc1: Scalars['Float'];
  lhc2: Scalars['Float'];
};

export type Fid50TxLhcGlowPlugAndHeadHeaterStatusInput = {
  glowPlug1: InputMaybe<Scalars['Float']>;
  glowPlug2: InputMaybe<Scalars['Float']>;
  headHeater1: InputMaybe<Scalars['Float']>;
  headHeater2: InputMaybe<Scalars['Float']>;
  lhc1: InputMaybe<Scalars['Float']>;
  lhc2: InputMaybe<Scalars['Float']>;
};

export type Fid50TxPackage = {
  __typename?: 'Fid50TxPackage';
  ch1AirPressureSetpoint: Scalars['Float'];
  ch1CpPressureSetpoint: Scalars['Float'];
  ch1FidPressureSetpoint: Scalars['Float'];
  ch1FuelPressureSetpoint: Scalars['Float'];
  ch1HeadTemperatureSetpoint: Scalars['Float'];
  ch1ProbeTemperatureSetpoint: Scalars['Float'];
  ch1Solenoids: Fid50TxChNSolenoids;
  ch2AirPressureSetpoint: Scalars['Float'];
  ch2HeadTemperatureSetpoint: Scalars['Float'];
  ch2ProbeTemperatureSetpoint: Scalars['Float'];
  ch2Solenoids: Fid50TxChNSolenoids;
  gainRangeAndHtStatus: Fid50TxGainRangeAndHtStatus;
  glowplugCurrent: Scalars['Float'];
  lhcGlowPlugAndHeadHeaterStatus: Fid50TxLhcGlowPlugAndHeadHeaterStatus;
  pumpRpmSetpoint: Scalars['Float'];
  span1: Scalars['Float'];
  span2: Scalars['Float'];
  userDigitalOutput: Fid50TxUserDigitalOutput;
  zero1: Scalars['Float'];
  zero2: Scalars['Float'];
};

export type Fid50TxPackageInput = {
  ch1AirPressureSetpoint: InputMaybe<Scalars['Float']>;
  ch1CpPressureSetpoint: InputMaybe<Scalars['Float']>;
  ch1FidPressureSetpoint: InputMaybe<Scalars['Float']>;
  ch1FuelPressureSetpoint: InputMaybe<Scalars['Float']>;
  ch1HeadTemperatureSetpoint: InputMaybe<Scalars['Float']>;
  ch1ProbeTemperatureSetpoint: InputMaybe<Scalars['Float']>;
  ch1Solenoids: InputMaybe<Fid50TxChNSolenoidsInput>;
  ch2AirPressureSetpoint: InputMaybe<Scalars['Float']>;
  ch2HeadTemperatureSetpoint: InputMaybe<Scalars['Float']>;
  ch2ProbeTemperatureSetpoint: InputMaybe<Scalars['Float']>;
  ch2Solenoids: InputMaybe<Fid50TxChNSolenoidsInput>;
  gainRangeAndHtStatus: InputMaybe<Fid50TxGainRangeAndHtStatusInput>;
  glowplugCurrent: InputMaybe<Scalars['Float']>;
  lhcGlowPlugAndHeadHeaterStatus: InputMaybe<Fid50TxLhcGlowPlugAndHeadHeaterStatusInput>;
  pumpRpmSetpoint: InputMaybe<Scalars['Float']>;
  span1: InputMaybe<Scalars['Float']>;
  span2: InputMaybe<Scalars['Float']>;
  userDigitalOutput: InputMaybe<Fid50TxUserDigitalOutputInput>;
  zero1: InputMaybe<Scalars['Float']>;
  zero2: InputMaybe<Scalars['Float']>;
};

export type Fid50TxUserDigitalOutput = {
  __typename?: 'Fid50TxUserDigitalOutput';
  isolated0: Scalars['Float'];
  isolated1: Scalars['Float'];
  isolated2: Scalars['Float'];
  isolated3: Scalars['Float'];
  nonIsolated0: Scalars['Float'];
  nonIsolated1: Scalars['Float'];
  nonIsolated2: Scalars['Float'];
  nonIsolated3: Scalars['Float'];
};

export type Fid50TxUserDigitalOutputInput = {
  isolated0: InputMaybe<Scalars['Float']>;
  isolated1: InputMaybe<Scalars['Float']>;
  isolated2: InputMaybe<Scalars['Float']>;
  isolated3: InputMaybe<Scalars['Float']>;
  nonIsolated0: InputMaybe<Scalars['Float']>;
  nonIsolated1: InputMaybe<Scalars['Float']>;
  nonIsolated2: InputMaybe<Scalars['Float']>;
  nonIsolated3: InputMaybe<Scalars['Float']>;
};

export type Fid50UserDigitalInputs = {
  __typename?: 'Fid50UserDigitalInputs';
  isolated0: Scalars['Float'];
  isolated1: Scalars['Float'];
  isolated2: Scalars['Float'];
  isolated3: Scalars['Float'];
  isolatedValue: Scalars['Float'];
  nonIsolated0: Scalars['Float'];
  nonIsolated1: Scalars['Float'];
  nonIsolated2: Scalars['Float'];
  nonIsolated3: Scalars['Float'];
  nonIsolatedValue: Scalars['Float'];
};

export type Fid50UserDigitalOutput = {
  __typename?: 'Fid50UserDigitalOutput';
  isolated0: Scalars['Float'];
  isolated1: Scalars['Float'];
  isolated2: Scalars['Float'];
  isolated3: Scalars['Float'];
  nonIsolated0: Scalars['Float'];
  nonIsolated1: Scalars['Float'];
  nonIsolated2: Scalars['Float'];
  nonIsolated3: Scalars['Float'];
};

export type Fid50UserDigitalOutputInput = {
  isolated0: InputMaybe<Scalars['Float']>;
  isolated1: InputMaybe<Scalars['Float']>;
  isolated2: InputMaybe<Scalars['Float']>;
  isolated3: InputMaybe<Scalars['Float']>;
  nonIsolated0: InputMaybe<Scalars['Float']>;
  nonIsolated1: InputMaybe<Scalars['Float']>;
  nonIsolated2: InputMaybe<Scalars['Float']>;
  nonIsolated3: InputMaybe<Scalars['Float']>;
};

export type GateCounter = {
  __typename?: 'GateCounter';
  count: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  castleCounterDecrement: CastleCounter;
  castleCounterIncrement: CastleCounter;
  gateCounterDecrement: GateCounter;
  gateCounterIncrement: GateCounter;
  setFid50SetPoints: Fid50SetPoints;
  setFid50TxPackage: Fid50TxPackage;
};


export type MutationSetFid50SetPointsArgs = {
  setPoints: Fid50SetPointsInput;
};


export type MutationSetFid50TxPackageArgs = {
  txPackage: Fid50TxPackageInput;
};

export type Query = {
  __typename?: 'Query';
  castleCounter: CastleCounter;
  fid50RxPackage: Fid50RxPackage;
  fid50SetPoints: Fid50SetPoints;
  fid50State: Fid50State;
  fid50TxPackage: Fid50TxPackage;
  gateCounter: GateCounter;
  sayHello: Scalars['String'];
};


export type QuerySayHelloArgs = {
  clientId: Scalars['String'];
  myMame: Scalars['String'];
};

export type Subscription = {
  __typename?: 'Subscription';
  castleCounterChanged: CastleCounter;
};

export type Fid50QueryVariables = Exact<{ [key: string]: never; }>;


export type Fid50Query = { __typename?: 'Query', fid50State: { __typename?: 'Fid50State', mainPcbUpTimeSecond: number, pumpPcbUpTimeSecond: number, pumpRpm: number }, fid50SetPoints: { __typename?: 'Fid50SetPoints', pumpRpmSetpoint: number, solenoids: { __typename?: 'Fid50Solenoids', vacuum: boolean } } };

export type SetPumpRpmSetpointMutationVariables = Exact<{
  value: InputMaybe<Scalars['Float']>;
}>;


export type SetPumpRpmSetpointMutation = { __typename?: 'Mutation', setFid50SetPoints: { __typename?: 'Fid50SetPoints', pumpRpmSetpoint: number } };

export type SetSolenoidsVacuumMutationVariables = Exact<{
  value: InputMaybe<Scalars['Boolean']>;
}>;


export type SetSolenoidsVacuumMutation = { __typename?: 'Mutation', setFid50SetPoints: { __typename?: 'Fid50SetPoints', solenoids: { __typename?: 'Fid50Solenoids', vacuum: boolean } } };

export type SayHelloQueryVariables = Exact<{ [key: string]: never; }>;


export type SayHelloQuery = { __typename?: 'Query', sayHello: string };

export type GetCastleCountQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCastleCountQuery = { __typename?: 'Query', castleCounter: { __typename?: 'CastleCounter', count: number } };

export type CastleCounterChangedSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type CastleCounterChangedSubscription = { __typename?: 'Subscription', castleCounterChanged: { __typename?: 'CastleCounter', count: number } };

export type CastleCounterIncrementMutationVariables = Exact<{ [key: string]: never; }>;


export type CastleCounterIncrementMutation = { __typename?: 'Mutation', castleCounterIncrement: { __typename?: 'CastleCounter', count: number } };

export type CastleCounterDecrementMutationVariables = Exact<{ [key: string]: never; }>;


export type CastleCounterDecrementMutation = { __typename?: 'Mutation', castleCounterDecrement: { __typename?: 'CastleCounter', count: number } };

export type GetGateCountQueryVariables = Exact<{ [key: string]: never; }>;


export type GetGateCountQuery = { __typename?: 'Query', gateCounter: { __typename?: 'GateCounter', count: number } };

export type GateCounterIncrementMutationVariables = Exact<{ [key: string]: never; }>;


export type GateCounterIncrementMutation = { __typename?: 'Mutation', gateCounterIncrement: { __typename?: 'GateCounter', count: number } };

export type GateCounterDecrementMutationVariables = Exact<{ [key: string]: never; }>;


export type GateCounterDecrementMutation = { __typename?: 'Mutation', gateCounterDecrement: { __typename?: 'GateCounter', count: number } };


      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {}
};
      export default result;
    

export const Fid50Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Fid50"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"fid50State"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"mainPcbUpTimeSecond"}},{"kind":"Field","name":{"kind":"Name","value":"pumpPcbUpTimeSecond"}},{"kind":"Field","name":{"kind":"Name","value":"pumpRpm"}}]}},{"kind":"Field","name":{"kind":"Name","value":"fid50SetPoints"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"solenoids"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"vacuum"}}]}},{"kind":"Field","name":{"kind":"Name","value":"pumpRpmSetpoint"}}]}}]}}]} as unknown as DocumentNode<Fid50Query, Fid50QueryVariables>;
export const SetPumpRpmSetpointDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"SetPumpRpmSetpoint"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"value"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Float"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"setFid50SetPoints"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"setPoints"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"pumpRpmSetpoint"},"value":{"kind":"Variable","name":{"kind":"Name","value":"value"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"pumpRpmSetpoint"}}]}}]}}]} as unknown as DocumentNode<SetPumpRpmSetpointMutation, SetPumpRpmSetpointMutationVariables>;
export const SetSolenoidsVacuumDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"SetSolenoidsVacuum"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"value"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"setFid50SetPoints"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"setPoints"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"solenoids"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"vacuum"},"value":{"kind":"Variable","name":{"kind":"Name","value":"value"}}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"solenoids"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"vacuum"}}]}}]}}]}}]} as unknown as DocumentNode<SetSolenoidsVacuumMutation, SetSolenoidsVacuumMutationVariables>;
export const SayHelloDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"SayHello"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"sayHello"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"myMame"},"value":{"kind":"StringValue","value":"rh","block":false}},{"kind":"Argument","name":{"kind":"Name","value":"clientId"},"value":{"kind":"StringValue","value":"one","block":false}}]}]}}]} as unknown as DocumentNode<SayHelloQuery, SayHelloQueryVariables>;
export const GetCastleCountDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetCastleCount"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounter"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GetCastleCountQuery, GetCastleCountQueryVariables>;
export const CastleCounterChangedDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"subscription","name":{"kind":"Name","value":"CastleCounterChanged"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounterChanged"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<CastleCounterChangedSubscription, CastleCounterChangedSubscriptionVariables>;
export const CastleCounterIncrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CastleCounterIncrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounterIncrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<CastleCounterIncrementMutation, CastleCounterIncrementMutationVariables>;
export const CastleCounterDecrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CastleCounterDecrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"castleCounterDecrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<CastleCounterDecrementMutation, CastleCounterDecrementMutationVariables>;
export const GetGateCountDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetGateCount"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gateCounter"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"client"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GetGateCountQuery, GetGateCountQueryVariables>;
export const GateCounterIncrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"GateCounterIncrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gateCounterIncrement"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"client"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GateCounterIncrementMutation, GateCounterIncrementMutationVariables>;
export const GateCounterDecrementDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"GateCounterDecrement"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gateCounterDecrement"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"client"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<GateCounterDecrementMutation, GateCounterDecrementMutationVariables>;