// Hey Emacs, this is -*- coding: utf-8 -*-

import { createContext, useContext } from 'react';

export class AppContextValue {
  readonly xxx = 'xxx';
}

export const AppContext = createContext<AppContextValue>(
  null as unknown as AppContextValue,
);

export const useAppContext = (): AppContextValue => useContext(AppContext);
