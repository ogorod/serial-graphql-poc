// Hey Emacs, this is -*- coding: utf-8 -*-

// Apollo useQuery does not work well with Fast Refresh:
// https://nextjs.org/docs/basic-features/fast-refresh
// @refresh reset

import { useMemo } from 'react';

import type { NextPage } from 'next';

import Head from 'next/head';

import { useQuery } from '@apollo/client';

import { Grid, Row, Col } from '@zendeskgarden/react-grid';

import { Fid50Context } from '~/lib/front/fid50-context';
import type { Fid50ContextValue } from '~/lib/front/fid50-context';

// import { CambustionChrome } from '~/components/cambustion-chrome';

import { SpacedWell } from '~/components/styled/wells';
import { H1, H2 } from '~/components/styled/typography';
import { Root, Main } from '~/components/styled/root';

import {
  MainPcbUpTimeSecondDigitalDisplay,
  PumpPcbUpTimeSecondDigitalDisplay,
  PumpRpmDigitalDisplay,
} from '~/components/digital-displays';

import { PumpRpmSetpointDigitalEntry } from '~/components/setpoint-digital-entries';

import { SolenoidsVacuumToggleEntry } from '~/components/setpoint-toggle-entries';

import { Fid50Document } from '~/lib/front/graphql-operations';

const Home: NextPage = () => {
  const { data: fid50Query } = useQuery(Fid50Document, {
    ssr: false,
    pollInterval: 500,
    errorPolicy: 'all',
  });

  const fid50ContextValue = useMemo<Fid50ContextValue>(
    () => ({ fid50Query }),
    [fid50Query],
  );

  return (
    <Root>
      <Head>
        <title>FID 50 Under the Bonnet</title>
        <meta name="description" content="FID 50 Under the Bonnet" />
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>

      <Main>
        <H1>FID 50 Under Bonnet</H1>
        <Fid50Context.Provider value={fid50ContextValue}>
          <Grid debug>
            <Row>
              <Col xs={12} sm={6} md={6} lg={4} xl={3}>
                <SpacedWell>
                  <H2>Misc Controls</H2>
                  <MainPcbUpTimeSecondDigitalDisplay />
                  <PumpPcbUpTimeSecondDigitalDisplay />
                  <PumpRpmDigitalDisplay />
                  <PumpRpmSetpointDigitalEntry />
                  <SolenoidsVacuumToggleEntry />
                </SpacedWell>
              </Col>
            </Row>
          </Grid>
        </Fid50Context.Provider>
      </Main>
    </Root>
  );
};

export default Home;
