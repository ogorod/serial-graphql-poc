// Hey Emacs, this is -*- coding: utf-8 -*-

// import '~/styles/globals.css';

import { useEffect, useState } from 'react';

// import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import { ThemeProvider as ZenThemeProvider } from '@zendeskgarden/react-theming';

import { ApolloProvider } from '@apollo/client';

import '@zendeskgarden/css-bedrock';
import '~/styles/globals.css';

import type { AppProps } from 'next/app';

import { getApolloClient } from '~/client/get-apollo-client';
import { AppContext, AppContextValue } from '~/lib/front/app-context';

// const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  const [appContextValue] = useState(() => new AppContextValue());

  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <AppContext.Provider value={appContextValue}>
      <ApolloProvider client={getApolloClient(appContextValue)}>
        {/* <StyledThemeProvider theme={theme}> */}
        <ZenThemeProvider>
          <Component {...pageProps} />
        </ZenThemeProvider>
        {/* </StyledThemeProvider> */}
      </ApolloProvider>
    </AppContext.Provider>
  );
};

export default MyApp;
