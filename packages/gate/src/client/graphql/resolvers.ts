// Hey Emacs, this is -*- coding: utf-8 -*-

import { Resolvers as ApolloResolvers } from '@apollo/client';
import { InMemoryCache } from '@apollo/client/cache';

import type {
  Resolvers as TypedResolvers,
  GateCounter,
} from './resolver-types';

import type { GetGateCountQuery } from './resolver-operations';

import { GetGateCountDocument } from './resolver-operations';

const updateGateCounter = (
  cache: InMemoryCache,
  getNewValueFn: (value: number) => number,
): GateCounter => {
  const { gateCounter: prevGateCounter } = cache.readQuery({
    query: GetGateCountDocument,
  }) as GetGateCountQuery;

  const gateCounter = {
    ...prevGateCounter,
    count: getNewValueFn(prevGateCounter.count),
  };

  cache.writeQuery({
    query: GetGateCountDocument,
    data: { gateCounter },
  });

  return gateCounter;
};

const typedResolvers: TypedResolvers = {
  Mutation: {
    gateCounterIncrement: (_root, _args, { cache }): GateCounter => {
      return updateGateCounter(cache, (value) => value + 1);
    },

    gateCounterDecrement: (_root, _args, { cache }): GateCounter => {
      return updateGateCounter(cache, (value) => value - 1);
    },
  },
};

export const resolvers = typedResolvers as ApolloResolvers;

export const data = {
  gateCounter: {
    query: GetGateCountDocument,
    data: {
      gateCounter: {
        __typename: 'GateCounter',
        count: 0,
      },
    },
  },
};
