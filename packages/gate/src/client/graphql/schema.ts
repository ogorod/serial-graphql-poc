// Hey Emacs, this is -*- coding: utf-8 -*-

import schema from './schema.graphql';
import { resolvers, data } from './resolvers';

const typeDefs = schema;

export { typeDefs, resolvers, data };
