// Hey Emacs, this is -*- coding: utf-8 -*-

import type { InMemoryCache } from '@apollo/client/cache';

export interface Context {
  cache: InMemoryCache;
}
