// Hey Emacs, this is -*- coding: utf-8 -*-

/* eslint-disable @typescript-eslint/no-empty-interface */

import 'styled-components';
import type { IGardenTheme } from '@zendeskgarden/react-theming';

declare module 'styled-components' {
  export interface DefaultTheme extends IGardenTheme {}
}
