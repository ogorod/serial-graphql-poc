// Hey Emacs, this is -*- coding: utf-8 -*-

// Taken from https://github.com/DefinitelyTyped/DefinitelyTyped/issues/31245#issuecomment-497038332

import type { CSSProp } from 'styled-components';

declare module 'react' {
  interface Attributes {
    css?: CSSProp;
  }
}
