// Hey Emacs, this is -*- coding: utf-8 -*-

declare module '*.graphql' {
  import type { DocumentNode } from 'graphql';

  const Schema: DocumentNode;
  export = Schema;
}
