/** @type {import('next').NextConfig} */

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withBundleAnalyzer({
  reactStrictMode: true,

  // see https://github.com/vercel/next.js/issues/30802
  // styledComponent: true,
  // experimental: {
  //   styledComponent: true,
  // },

  webpack: (config, { isServer }) => {
    config.module.rules.push({
      test: /\.(graphql|gql)$/,
      exclude: /node_modules/,
      loader: 'graphql-tag/loader',
    });

    if (!isServer) {
      config.module.rules.push({
        test: /\/lib\/back\//,
        use: 'null-loader',
      });

      config.module.rules.push({
        test: /\/back-.*\//,
        use: 'null-loader',
      });
    }

    return config;
  },
});
